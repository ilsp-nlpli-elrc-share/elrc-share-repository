from django import template
from django.contrib.auth.models import Group

from metashare.accounts.models import EditorGroup

register = template.Library()


@register.filter(name='is_member')
def is_member(user, group_name):
    group = Group.objects.get(name=group_name)
    return True if group in user.groups.all() else False


@register.filter(name='is_member_in_editor_groups')
def is_member_in_editor_groups(user):
    editor_groups = EditorGroup.objects.all().values_list('name', flat=True)
    user_groups = user.groups.all().values_list('name', flat=True)
    return True if set(editor_groups).intersection(set(user_groups)) else False
