import xmltodict
import json
from collections import OrderedDict

from metashare.utils import delete_keys_from_dict
from metashare.xml_utils import to_xml_string

customdecoder = json.JSONDecoder(object_pairs_hook=OrderedDict)


def xml_to_json(obj):
    list_fields = ['distributionInfo', 'licenceInfo', 'corpusTextInfo',
                   'distributionMedium', 'downloadLocation', 'executionLocation',
                   'attributionText', 'iprHolder', 'contactPerson', 'surname', 'languageInfo']

    # get xml representation
    xml_string = to_xml_string(obj.export_to_elementtree(), encoding="utf-8").encode("utf-8")

    # parse xml to dict
    dict_repr = xmltodict.parse(xml_string, force_list=list_fields)
    return json.dumps(dict_repr, indent=4).encode('utf8')


def json_to_xml(json_string):
    print json.loads(json_string, object_pairs_hook=OrderedDict)
    return xmltodict.unparse(json.loads(json_string, object_pairs_hook=OrderedDict), pretty=True).encode('utf-8')


def xml_to_json_pruned(obj):
    j = xml_to_json(obj)
    nj = customdecoder.decode(j)
    pruned = delete_keys_from_dict(
        nj, ['contactPerson', '@xmlns', '@xmlns:xsi', '@xsi:schemaLocation', 'metadataCreator', 'communicationInfo']
    )
    return json.dumps(pruned, indent=4, ensure_ascii=False).encode('utf8')
