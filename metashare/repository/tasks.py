from datetime import datetime
from django.core.mail import EmailMessage

from metashare.local_settings import ILSP_ADMINS
from metashare.processing.celery_app import app


@app.task(name="send-agreement-email", ignore_result=False, bind=True)
def send_agreement_email(self, file_obj, user_email):
    email_subject = "[ELRC-SHARE] Copy of your ELRC-SHARE Resource Deposition Agreement"
    msg_body = "Dear ELRC-SHARE user,\n\nPlease find attached a copy of your ELRC-SHARE Resource Deposition " \
               "Agreement, submitted today, {}.\n\nThe ELRC-SHARE team".format(datetime.now().strftime("%Y-%m-%d"))
    msg = EmailMessage(email_subject, msg_body,
                       from_email='elrc-share@ilsp.gr', to=[user_email], bcc=[ILSP_ADMINS])
    msg.attach("Resource_Deposition_Agreement.pdf", file_obj,
               "application/pdf")
    msg.send()
