import os

from PyPDF2.pdf import BytesIO
from xhtml2pdf import pisa
import cStringIO as StringIO
from django.template.loader import get_template
from django.template import Context

from metashare import settings
from metashare.settings import ROOT_PATH


def link_callback(uri, rel):
    # use short variable names
    sUrl = settings.STATIC_URL  # Typically /static/
    sRoot = settings.STATIC_ROOT  # Typically /home/userX/project_static/

    # convert URIs to absolute system paths
    print uri
    path = os.path.join(sRoot, uri.replace(sUrl, ""))
    if uri.startswith('/static/'):
        print uri
        path = os.path.join('/opt/ELRC2/metashare/static/', uri.replace(sUrl, ""))

    # make sure that file exists
    if not os.path.isfile(path):
        raise Exception('media URI must start with %s' % (sUrl))
    return path


def html_to_pdf(context_update):
    """
    printf '#!/bin/bash\nxvfb-run -a --server-args="-screen 0, 1024x768x24" /usr/bin/wkhtmltopdf -q $*' > /usr/bin/wkhtmltopdf.sh
    chmod a+x /usr/bin/wkhtmltopdf.sh
    ln -s /usr/bin/wkhtmltopdf.sh /usr/local/bin/wkhtmltopdf
    """
    template = get_template("repository/editor/ipr_agreement.html")
    context = Context({'pagesize': 'A4'})
    context.update(context_update)
    html = template.render(context)
    result = open('{}/Resource_Deposition_Agreement.pdf'.format(ROOT_PATH), 'wb')
    pisa.pisaDocument(StringIO.StringIO(html.encode('utf-8')), dest=result, link_callback=link_callback)
    result.close()
    return result
