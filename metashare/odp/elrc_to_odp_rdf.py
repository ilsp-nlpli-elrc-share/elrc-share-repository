import uuid

from rdflib import Graph, Literal, RDF, URIRef
# rdflib knows about some namespaces, like FOAF
from rdflib.namespace import FOAF, XSD, DCAT, DCTERMS, Namespace

from metashare.bcp47 import iana
from metashare.odp.maps import get_lang_uri, get_domain_uri, licence_map
from metashare.report_utils.report_utils import _get_resource_mimetypes, _get_resource_domain_info, \
    _get_resource_lang_info
from metashare.repository.models import resourceInfoType_model
from metashare.settings import DJANGO_URL
from metashare.repository.decorators import _resource_is_downloadable

r = resourceInfoType_model.objects.filter(storage_object__deleted=False).first()

DCATAPOP = Namespace('http://data.europa.eu/88u/ontology/dcatapop#')

PUBLISHER = URIRef('http://publications.europa.eu/resource/authority/corporate-body/CNECT')


def _get_licence(resource):
    distribution = resource.distributioninfotype_model_set.all()[0]
    licence = distribution.licenceInfo.all()[0].licence
    return licence


def _get_candidates():
    result = list()
    for resource in resourceInfoType_model.objects.filter(
            storage_object__deleted=False,
            storage_object__publication_status='p',
            management_object__delivered_odp=False):
        if _resource_is_downloadable(resource) and _get_licence(resource) in licence_map.keys():
            result.append(resource)
    return result


def build_graph(resource):
    # create a Graph
    g = Graph()

    # Bind Namespaces to prefixes
    g.bind('dcat', DCAT)
    g.bind('dcterms', DCTERMS)
    g.bind('dcatapop', DCATAPOP)
    g.bind("foaf", FOAF)

    # Create an RDF URI node to use as the subject for multiple triples
    dataset = URIRef('http://data.europa.eu/88u/dataset/elrc_{}'.format(resource.id))

    dist_id = 'http://data.europa.eu/88u/dataset/elrc_distribution_{}_{}'.format(resource.id, uuid.uuid4().hex)
    distribution = URIRef(dist_id)
    licence = licence_map.get(_get_licence(resource))
    addition = 'This dataset has been created within the framework of the European Language Resource Coordination ' \
               '(ELRC) Connecting Europe Facility - Automated Translation (CEF.AT) actions SMART 2014/1074, ' \
               'SMART 2015/1091 and SMART 2019/1083. For further information on the project: http://lr-coordination.eu.'

    # Add triples using store's add() method.
    # Get titles in all available languages
    for lang, value in resource.identificationInfo.resourceName.iteritems():
        g.add((dataset, DCTERMS.title, Literal(value, lang=lang)))
    for lang, value in resource.identificationInfo.description.iteritems():
        if lang == 'en':
            g.add((dataset, DCTERMS.description, Literal("{}\n\n{}".format(value.strip(), addition), lang=lang)))
        else:
            g.add((dataset, DCTERMS.description, Literal(value.strip(), lang=lang)))
    for lang in _get_resource_lang_info(resource):
        g.add((dataset, DCTERMS.language, URIRef(get_lang_uri(iana.get_language_subtag(lang)))))

    g.add((dataset, DCTERMS.identifier, Literal('ELRC_{}'.format(resource.id))))
    g.add((dataset, DCATAPOP.isPartOfCatalog, URIRef("http://data.europa.eu/88u/catalog/euodp")))
    g.add((dataset, DCTERMS.publisher, PUBLISHER))
    g.add((dataset, RDF.type, URIRef('http://www.w3.org/ns/dcat#Dataset')))
    g.add((dataset, DCATAPOP.datasetGroup, URIRef('resources-for-language-technologies')))
    g.add((dataset, DCATAPOP.ckanName, Literal('elrc_{}'.format(resource.id))))
    g.add((dataset, DCAT.distribution, distribution))
    g.add((dataset, DCTERMS.subject, URIRef('http://eurovoc.europa.eu/4478')))
    g.add((dataset, DCTERMS.subject, URIRef('http://eurovoc.europa.eu/1670')))

    for domain in _get_resource_domain_info(resource):
        theme = get_domain_uri(domain)
        if theme:
            g.add((dataset, DCAT.theme, URIRef(theme)))

    data_formats = [df for df in _get_resource_mimetypes(resource)]

    g.add((distribution, DCTERMS.license, URIRef(licence)))
    g.add((distribution, DCTERMS.type,
           URIRef('http://publications.europa.eu/resource/authority/distribution-type/DOWNLOADABLE_FILE')))
    g.add((distribution, DCTERMS['format'], URIRef('http://publications.europa.eu/resource/authority/file-type/ZIP')))
    g.add((distribution, DCTERMS.description, Literal("Data archive containing files in the following formats: {}"
                                                      .format(", ".join(data_formats)), lang='en')))
    g.add((distribution, DCTERMS.title, Literal("Data archive containing files in the following formats: {}"
                                                .format(", ".join(data_formats)), lang='en')))
    g.add((distribution, RDF.type, URIRef('http://www.w3.org/ns/dcat#Distribution')))
    g.add((distribution, DCAT.accessURL, URIRef("{}{}".format(DJANGO_URL, resource.get_absolute_url()))))
    return g.serialize(format='xml').decode("utf-8")


def post_to_odp(resource, status='published'):
    data = {
        "addReplaces": [
            {
                "objectUri": "http://data.europa.eu/88u/dataset/elrc_{}".format(resource.id),
                "addReplace": {
                    "objectStatus": status
                }
            }
        ],
        "rdfFile": str(build_graph(resource))
    }

    return data
