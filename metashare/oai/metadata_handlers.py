# coding=utf-8
import logging

from django.conf import settings
from lxml import etree

# Setup logging support.
LOGGER = logging.getLogger(__name__)
LOGGER.addHandler(settings.LOG_HANDLER)


def _parse_from_unicode(unicode_str):
    """
    Parses the string and returns an etree tree
    """
    utf8_parser = etree.XMLParser(
        encoding='utf-8',
        remove_blank_text=True,
        ns_clean=True)
    encoded_str = unicode_str.encode('utf-8')
    return etree.fromstring(encoded_str, parser=utf8_parser)


def _transform_xml(xml, xslt_path):
    """
    Converts the xml document from one metadata schema to another
    """
    with open('{0}/../misc/tools/{1}'.format(
            settings.ROOT_PATH, xslt_path)) as xslt_file:
        xslt_root = etree.parse(xslt_file)
    transform = etree.XSLT(xslt_root)
    return transform(xml)


# class MetashareWriter:
#     """
#     Implementation of a writer. A writer takes
#     a takes a metadata object and produces a chunk
#     of XML in the right format for this metadata.
#     """
#
#     def __init__(self):
#         pass
#
#     def __call__(self, element, metadata):
#         map_ = metadata.getMap()
#         # map contains only the root Element of the ElementTree
#         root_elem = map_['resourceInfo']
#         element.append(root_elem.getroot())


class ELGWriter:
    """
    Implementation of a writer. A writer takes
    a takes a metadata object and produces a chunk
    of XML in the right format for this metadata.
    """

    def __init__(self):
        pass

    def __call__(self, element, metadata):
        map_ = metadata.getMap()
        # map contains only the root Element of the ElementTree
        root_elem = map_['resourceInfo']
        element.append(root_elem.getroot())


class DCWriter:
    """
    Implementation of a writer. A writer takes
    a takes a metadata object and produces a
    chunk of XML in the right format for this metadata.
    """

    def __init__(self):
        pass

    def __call__(self, element, metadata):
        map_ = metadata.getMap()
        # map contains only the ElementTree
        root_elem = map_['metadataInfo']
        try:
            element.append(root_elem.getroot())
        except:
            pass


class ELGMap:
    """
    """

    def __init__(self):
        pass

    def getMap(self, raw_xml_record):
        # in solr oai index, when a record is deleted, we don't index the metadata, so we don't need to transform
        # anything
        if raw_xml_record:
            map_ = {}
            root = _parse_from_unicode(raw_xml_record)
            tree = root.getroottree()
            xslt_path = 'ELGConverters/ELRC2ELG.xsl'
            elg_root = _transform_xml(tree, xslt_path)
            map_['resourceInfo'] = elg_root
            return map_
        return None


class DCMap:
    """
    """

    def __init__(self):
        pass

    def getMap(self, raw_xml_record):
        if raw_xml_record:
            map_ = {}
            root = _parse_from_unicode(raw_xml_record)
            tree = root.getroottree()
            xslt_path = 'DCConverters/metashareToDC.xsl'
            dc_root = _transform_xml(tree, xslt_path)
            map_['metadataInfo'] = dc_root
            return map_
        return None



# class OlacWriter:
#     """
#     Implementation of a writer. A writer takes
#     a takes a metadata object and produces a
#     chunk of XML in the right format for this metadata.
#     """
#
#     def __init__(self):
#         pass
#
#     def __call__(self, element, metadata):
#         map_ = metadata.getMap()
#         # map contains only the ElementTree
#         root_elem = map_['metadataInfo']
#         try:
#             element.append(root_elem.getroot())
#         except:
#             pass


# class CmdiWriter:
#     """
#     Implementation of a writer. A writer takes
#     a takes a metadata object and produces a
#     chunk of XML in the right format for this metadata.
#     """
#
#     def __init__(self):
#         pass
#
#     def __call__(self, element, metadata):
#         map_ = metadata.getMap()
#         # map contains only the root element
#         root_elem = map_['CMD']
#         element.append(root_elem)


# class EDMWriter:
#     """
#     Implementation of a writer. A writer takes
#     a takes a metadata object and produces a
#     chunk of XML in the right format for this metadata.
#     """
#
#     def __init__(self):
#         pass
#
#     def __call__(self, element, metadata):
#         map_ = metadata.getMap()
#         # map contains only the ElementTree
#         root_elem = map_['metadataInfo']
#         try:
#             element.append(root_elem.getroot())
#         except:
#             pass


# class MetashareMap:
#     """
#     """
#
#     def __init__(self):
#         pass
#
#     def getMap(self, raw_xml_record):
#         map_ = {}
#         root = _parse_from_unicode(raw_xml_record)
#         tree = root.getroottree()
#         map_['resourceInfo'] = tree
#         return map_

# class OlacMap:
#     """
#     """
#
#     def __init__(self):
#         pass
#
#     def getMap(self, raw_xml_record):
#         map_ = {}
#         root = _parse_from_unicode(raw_xml_record)
#         tree = root.getroottree()
#         xslt_path = 'OLACConverters/metashareToOlac.xsl'
#         olac_root = _transform_xml(tree, xslt_path)
#         map_['metadataInfo'] = olac_root
#         return map_


# class CmdiMap:
#     """
#     """
#
#     def __init__(self):
#         pass
#
#     def getMap(self, raw_xml_record):
#         map_ = {}
#         root = _parse_from_unicode(raw_xml_record)
#         tree = root.getroottree()
#         xslt_path = 'CMDIConverters/metashareToCmdi.xsl'
#         cmdi_root = _transform_xml(tree, xslt_path)
#         map_['CMD'] = cmdi_root
#         return map_


# class EDMMap:
#     """
#     """
#
#     def __init__(self):
#         pass
#
#     def getMap(self, raw_xml_record):
#         map_ = {}
#         root = _parse_from_unicode(raw_xml_record)
#         tree = root.getroottree()
#         xslt_path = 'EDMConverters/metashareToEDM.xsl'
#         edm_root = _transform_xml(tree, xslt_path)
#         map_['metadataInfo'] = edm_root
#         return map_
