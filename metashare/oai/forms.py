from django import forms

from metashare.oai.server import (
    DC, ELG
)


class ExposeForm(forms.Form):
    """
    Settings form for OAI-PMH import.
    """

    VERBS = (
        (u'Identify', u'Identify Server'),
        (u'GetRecord', u'Get Record'),
        (u'ListIdentifiers', u'List Identifiers'),
        (u'ListMetadataFormats', u'List Formats'),
        (u'ListRecords', u'List Records'),
        (u'ListSets', u'List Sets'),
    )
    
    FORMATS = (
        (u'', u'-----------'),
        (DC, DC),
        (ELG, ELG),
        # (METASHARE, METASHARE),
        # (OLAC, OLAC),
        # (CMDI, CMDI)
    )

    verb = forms.ChoiceField(
        label=u"What to do",
        choices=VERBS,
        help_text=u"Select OAI-PMH verb "
                  u"(fill out item identifier if applicable)",)

    metadata_str = forms.ChoiceField(
        label=u"OAI-PMH metadata format",
        choices=FORMATS,
        help_text=u"Select a metadata format if applicable with the specified verb",
        required=False,)

    itemid = forms.URLField(
        label=u"Resource PID",
        help_text=u"The Persistent Identifier of the resource. "
                  u"Leave empty for the whole collection or if not applicable",
        required=False,)
    
    from_ = forms.CharField(
        label=u"From",
        help_text=u"Lower bound for datestamp-based selective "
                  u"harvesting. e.g '2011-02-01T12:00:00Z' or '2011-02-01'",
        required=False,)
    
    until = forms.CharField(
        label=u"Until",
        help_text=u"Upper bound for datestamp-based selective "
                  u"harvesting. e.g '2020-02-01T12:00:00Z' or '2020-02-01'",
        required=False,)
                
    set_ = forms.CharField(
        label=u"Set",
        help_text=u"Select a set name to specify set criteria "
                  u"for selective harvesting",
        required=False,)
    
    resumptionToken = forms.CharField(
        label=u"Resumption Token",
        help_text=u"Exclusive argument. "
                  u"It is a token returned by a previous request "
                  u"that issued an incomplete list.",
        required=False,)
