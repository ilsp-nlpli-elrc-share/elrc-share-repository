from django.conf.urls import patterns, include

urlpatterns = patterns(
        'metashare.oai.views',
        (r'^$', 'oaipmh_view'),
        (r'^expose/$', 'expose'),
        (r'^search/', include('haystack.urls')),
)
