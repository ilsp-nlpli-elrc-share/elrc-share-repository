import logging

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

from metashare.oai import oaipmh_server
from metashare.oai.forms import ExposeForm

# Setup logging support.
LOGGER = logging.getLogger(__name__)
LOGGER.addHandler(settings.LOG_HANDLER)

ACCEPTED_REMOTE_ADDRESSES = []


@login_required
def expose(request):
    """
    Renders OAI-PMH expose page
    """
    form = ExposeForm(request.POST or None)
    if form.is_valid():
        # construct the url, that requests to the oai server
        verb = form.cleaned_data['verb']
        success_url = '{}?verb={}'.format(settings.OAIPMH_URL, verb)
        itemid = form.cleaned_data['itemid']
        if itemid:
            success_url += "&identifier={}".format(itemid)
        metadata_str = form.cleaned_data['metadata_str']
        if metadata_str:
            success_url += "&metadataPrefix={}".format(metadata_str)
        from_ = form.cleaned_data['from_']
        if from_:
            success_url += "&from={}".format(from_)
        until = form.cleaned_data['until']
        if until:
            success_url += "&until={}".format(until)
        set_ = form.cleaned_data['set_']
        if set_:
            success_url += "&set={}".format(set_)
        resumptionToken = form.cleaned_data['resumptionToken']
        if resumptionToken:
            success_url += "&resumptionToken={}".format(resumptionToken)
        return redirect(success_url)

    return render_to_response(
        'oaipmh/expose.html',
        {'form': form},
        context_instance=RequestContext(request))


def oaipmh_view(request):
    """
    Renders the response of the OAI-PMH Server
    """

    def get_client_ip(request):
        """
        Tracks the remote IP adress
        """
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip

    remote_addr = get_client_ip(request)
    if len(ACCEPTED_REMOTE_ADDRESSES) == 0 \
            or remote_addr in ACCEPTED_REMOTE_ADDRESSES:
        request_kw = {}
        verb = (request.GET.get("verb") or request.POST.get("verb"))
        if verb:
            request_kw['verb'] = verb
        metadataPrefix = (
                request.GET.get("metadataPrefix")
                or request.POST.get("metadataPrefix"))
        if metadataPrefix:
            request_kw['metadataPrefix'] = metadataPrefix
        from_ = (request.GET.get("from") or request.POST.get("from"))
        if from_:
            request_kw['from'] = from_
        until = (request.GET.get("until") or request.POST.get("until"))
        if until:
            request_kw['until'] = until
        set_ = (request.GET.get("set") or request.POST.get("set"))
        if set_:
            request_kw['set'] = set_
        identifier = (
                request.GET.get("identifier")
                or request.POST.get("identifier"))
        if identifier:
            request_kw['identifier'] = identifier
        res_token = (
                request.GET.get("resumptionToken")
                or request.POST.get("resumptionToken"))
        if res_token:
            request_kw['resumptionToken'] = res_token

        content = oaipmh_server.handleRequest(request_kw)
    else:
        LOGGER.warning(u"Client [%s] requests the resources via OAI-PMH" % remote_addr)
        raise PermissionDenied

    return HttpResponse(content=content, status=200, content_type="text/xml")
