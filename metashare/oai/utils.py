from pysolr import Results

from metashare.report_utils.report_utils import _get_resource_mimetypes, _get_resource_linguality
from metashare.repository.models import LICENCEINFOTYPE_LICENCE_CHOICES

# OPEN_LICENCES = [x for x, y in LICENCEINFOTYPE_LICENCE_CHOICES['choices'] if x.startswith('CC')]
# OPEN_LICENCES.extend([u'openUnder-PSI', u'publicDomain', u'NLOD-1.0', u'EUPL-1.1', u'LO-OL-v2'])
#
# DATA_FORMATS = [[u'TMX'], [u'Term Base eXchange'], [u'TMX', u'CSV'], [u'CSV', u'TMX']]


def exportable_to_elg(resource):
    criteria = [resource.storage_object.publication_status == 'p']
    if resource.storage_object.deleted is False:
        # criteria.append(resource.distributioninfotype_model_set.all()[0].licenceInfo.all()[0].licence in
        # OPEN_LICENCES)
        criteria.append(resource.distributioninfotype_model_set.all()[0].allowsUsesBesidesDGT is True)
        # criteria.append(_get_resource_linguality(resource) in [[u'Bilingual'], [u'Monolingual']])
        # criteria.append(_get_resource_mimetypes(resource) in DATA_FORMATS)
        # criteria.append((resource.storage_object.get_download()
        #                  and resource.management_object.ipr_clearing == u'cleared') or
        #                 resource.distributioninfotype_model_set.all()[0].downloadLocation)
    return all(criteria)
