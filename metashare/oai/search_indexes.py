# pylint: disable-msg=C0103
from haystack import indexes, connections as haystack_connections, connection_router as haystack_connection_router
from haystack.indexes import SearchIndex, CharField, BooleanField, MultiValueField, DateTimeField

from metashare.oai.server import _get_setSpecs
from metashare.storage.models import StorageObject, PUBLISHED


def update_oai_index_entry(so_obj):
    """
    Updates/creates the search index entry for the given storage_object
    object.

    The appropriate search index is automatically chosen.
    """
    router_name = haystack_connection_router.for_write()
    if hasattr(router_name, '__iter__'):
        router_name = router_name[1]
    haystack_connections[router_name] \
        .get_unified_index().get_index(StorageObject) \
        .update_object(so_obj, router_name)


class OAIIndex(SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True)
    resource_id = indexes.IntegerField(stored=True, indexed=True)
    deleted = BooleanField(stored=True, indexed=True)
    specs = MultiValueField(stored=True, indexed=True)
    modified = DateTimeField(stored=True, indexed=False)
    metadata = CharField(stored=True, indexed=False)

    def get_model(self):
        return StorageObject

    def prepare_metadata(self, obj):
        if obj.check_metadata():
            update_oai_index_entry(obj)
        return obj.metadata

    def prepare_deleted(self, obj):
        return obj.deleted

    def prepare_specs(self, obj):
        return _get_setSpecs(obj)

    def prepare_modified(self, obj):
        return obj.modified.replace(tzinfo=None)

    def prepare_resource_id(self, obj):
        return obj.resourceinfotype_model_set.first().id

    # METHODS
    def index_queryset(self, using=None):
        """
        Returns the default QuerySet to index when doing a full index update.

        In our case this is a QuerySet containing only published resources that
        have not been deleted, yet.
        """
        return self.read_queryset()

    def read_queryset(self, using=None):
        """
        Returns the default QuerySet for read actions.

        In our case this is a QuerySet containing only published resources that
        have not been deleted, yet.
        """
        return self.get_model().objects.filter(publication_status=PUBLISHED)

    def should_update(self, instance, **kwargs):
        '''
        Only index resources that are at least ingested.
        In other words, do not index internal resources.
        '''
        return instance.publication_status == PUBLISHED
