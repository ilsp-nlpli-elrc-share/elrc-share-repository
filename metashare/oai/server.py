import logging
import pysolr
from datetime import datetime
from itertools import combinations

from django.conf import settings
from lxml import etree
from oaipmh import common
from oaipmh.error import IdDoesNotExistError, CannotDisseminateFormatError

from dateutil import parser
from oaipmh.server import XMLTreeServer

from metashare.local_settings import DJANGO_URL
from metashare.oai.metadata_handlers import (
    DCMap, ELGMap
)
from metashare.oai.utils import exportable_to_elg
from metashare.repository.models import resourceInfoType_model
from metashare.storage.models import StorageObject, PUBLISHED
from metashare.utils import orderedset

# Setup logging support.
LOGGER = logging.getLogger(__name__)
LOGGER.addHandler(settings.LOG_HANDLER)

# Accepted formats
# METASHARE = 'metashare'
ELG = 'elg'
# OLAC = 'olac'
DC = 'dc'


# CMDI = 'cmdi'
# EDM = 'EDM'


class OaiPmhServer:
    """
    A server that responds to messages by returning OAI-PMH compliant XML.
    """

    def __init__(self, env):
        """
        Settings, required arguments are `name`, `url`, `adminEmails`.
        """
        self.env = env

    def identify(self):
        """
        OAI-PMH verb, identify.
        See http://www.openarchives.org/OAI/openarchivesprotocol.html#Identify
        """
        return common.Identify(
            repositoryName=self.env["repositoryName"],
            baseURL=self.env["baseURL"],
            protocolVersion="2.0",
            adminEmails=self.env["adminEmails"],
            earliestDatestamp=datetime(2004, 1, 1),
            deletedRecord='transient',
            granularity='YYYY-MM-DDThh:mm:ssZ',
            compression=['identity'])

    def getRecord(self, metadataPrefix, identifier):
        """
        OAI-PMH verb, GetRecord. See
        http://www.openarchives.org/OAI/openarchivesprotocol.html#GetRecord
        """
        try:
            resource = resourceInfoType_model.objects.get(id=identifier.split('-')[1])
        except:
            raise IdDoesNotExistError(
                'Resource with PID: "%s" does not exist' % identifier)
        else:
            so = resource.storage_object
            # convert to timezone naive datetime
            modified = so.modified.replace(tzinfo=None)
            resource_sets = _get_setSpecs(so)
            deleted = so.deleted
            header = common.Header(
                resource, identifier, modified,
                resource_sets, deleted)
            if metadataPrefix == ELG:
                map_class = ELGMap()
                map_ = map_class.getMap(so.metadata)
            elif metadataPrefix == DC:
                map_class = DCMap()
                map_ = map_class.getMap(so.metadata)
            # if metadataPrefix == METASHARE:
            #     map_class = MetashareMap()
            #     map_ = map_class.getMap(so.metadata)
            # elif metadataPrefix == OLAC:
            #     map_class = OlacMap()
            #     map_ = map_class.getMap(so.metadata)
            # elif metadataPrefix == CMDI:
            #     map_class = CmdiMap()
            #     resource_id = resource.id
            #     map_ = _add_elements_to_cmdi_metadata(
            #         map_class.getMap(so.metadata),
            #         resource_id, identifier)
            # elif metadataPrefix == EDM:
            #     map_class = EDMMap()
            #     map_ = map_class.getMap(so.metadata)
            else:
                raise CannotDisseminateFormatError(
                    '"%s" metadata format is not supported' % metadataPrefix)
            metadata = common.Metadata(resource, map_)
            return header, metadata, None

    def _add_oai_record(self, result_dict, metadataPrefix, from_, until):
        # so = StorageObject.objects.get(id=result_dict['django_id'])
        resource = resourceInfoType_model.objects.get(id=result_dict['resource_id'])
        identifier = 'ELRC-{}'.format(resource.id)
        modified = parser.parse(result_dict['modified']).replace(tzinfo=None)
        deleted = result_dict['deleted']
        specs = result_dict['specs']
        metadata = result_dict['metadata']
        header = common.Header(resource, identifier, modified, specs, deleted)

        if datestampInRange(header, from_, until):
            if metadataPrefix == ELG:
                map_class = ELGMap()
                map_ = map_class.getMap(metadata)
            elif metadataPrefix == DC:
                map_class = DCMap()
                map_ = map_class.getMap(metadata)
            # if metadataPrefix == METASHARE:
            #     map_class = MetashareMap()
            #     map_ = map_class.getMap(metadata)
            # elif metadataPrefix == OLAC:
            #     map_class = OlacMap()
            #     map_ = map_class.getMap(metadata)
            # elif metadataPrefix == CMDI:
            #     map_class = CmdiMap()
            #     resource_id = resource.id
            #     map_ = _add_elements_to_cmdi_metadata(
            #         map_class.getMap(metadata),
            #         resource_id, identifier)
            # elif metadataPrefix == EDM:
            #     map_class = EDMMap()
            #     map_ = map_class.getMap(metadata)
            else:
                raise CannotDisseminateFormatError(
                    '"%s" metadata format is not supported' % metadataPrefix)
            metadata = common.Metadata(resource, map_)
            return (header, metadata, None)

    def listRecords(self, metadataPrefix=None, from_=None, until=None, set=None):
        """
        OAI-PMH verb, ListRecords.See
        http://www.openarchives.org/OAI/openarchivesprotocol.html#ListRecords
        """
        solr = pysolr.Solr('http://localhost:8983/solr/oai')
        LOGGER.info("Serving OAI Harvesting")

        # def add_record(result, so, resource_sets):
        #     resource = so.resourceinfotype_model_set.first()
        #     identifier = 'ELRC-{}'.format(resource.id)
        #     # convert to timezone naive datetime
        #     modified = so.modified.replace(tzinfo=None)
        #     deleted = so.deleted
        #     header = common.Header(resource, identifier, modified, resource_sets, deleted)
        #
        #     if datestampInRange(header, from_, until):
        #         if metadataPrefix == METASHARE:
        #             map_class = MetashareMap()
        #             map_ = map_class.getMap(so.metadata)
        #         elif metadataPrefix == ELG:
        #             map_class = ELGMap()
        #             map_ = map_class.getMap(so.metadata)
        #         elif metadataPrefix == DC:
        #             map_class = DCMap()
        #             map_ = map_class.getMap(so.metadata)
        #         elif metadataPrefix == OLAC:
        #             map_class = OlacMap()
        #             map_ = map_class.getMap(so.metadata)
        #         elif metadataPrefix == CMDI:
        #             map_class = CmdiMap()
        #             resource_id = resource.id
        #             map_ = _add_elements_to_cmdi_metadata(
        #                 map_class.getMap(so.metadata),
        #                 resource_id, identifier)
        #         elif metadataPrefix == EDM:
        #             map_class = EDMMap()
        #             map_ = map_class.getMap(so.metadata)
        #         else:
        #             raise CannotDisseminateFormatError(
        #                 '"%s" metadata format is not supported' % metadataPrefix)
        #         metadata = common.Metadata(resource, map_)
        #         result.append((header, metadata, None))

        # storage_objects = get_storage_objects(from_, until)
        if set:
            search_result = solr.search(q='*:*', fq='specs:{}'.format(set), rows=StorageObject.objects.all().count())
        else:
            search_result = solr.search(q='*:*', rows=StorageObject.objects.all().count())

        for sr in list(search_result):
            try:
                modified = datetime.strptime(sr.get("modified"), "%Y-%m-%dT%H:%M:%S.%fZ")
            except ValueError:
                modified = datetime.strptime(sr.get("modified"), "%Y-%m-%dT%H:%M:%S%fZ")
            if from_ and modified >= from_:
                yield self._add_oai_record(sr, metadataPrefix, None, until)
        # if set:
        #     for so in storage_objects:
        #         if so.resourceinfotype_model_set.first():
        #             resource_sets = _get_setSpecs(so)
        #             if set in resource_sets:
        #                 add_record(result, so, resource_sets)
        # else:
        #     for so in storage_objects:
        #         if so.resourceinfotype_model_set.first():
        #             resource_sets = _get_setSpecs(so)
        #             add_record(result, so, resource_sets)

        # return result

    def listIdentifiers(self, metadataPrefix=None, from_=None, until=None, set=None):
        """
        OAI-PMH verb, ListIdentifiers. See
        http://www.openarchives.org/OAI/openarchivesprotocol.html#ListIdentifiers
        """

        def add_header(result, so, resource_sets):
            resource = so.resourceinfotype_model_set.first()
            identifier = 'ELRC-{}'.format(resource.id)
            # convert to timezone naive datetimes
            modified = so.modified.replace(tzinfo=None)
            deleted = so.deleted
            header = common.Header(resource, identifier, modified, resource_sets, deleted)
            if datestampInRange(header, from_, until):
                result.append(header)

        storage_objects = get_storage_objects(from_, until)
        result = []
        if set:
            for so in storage_objects:
                if so.resourceinfotype_model_set.first():
                    resource_sets = _get_setSpecs(so)
                    if set in resource_sets:
                        add_header(result, so, resource_sets)
        else:
            for so in storage_objects:
                if so.resourceinfotype_model_set.first():
                    resource_sets = _get_setSpecs(so)
                    add_header(result, so, resource_sets)
        return result

    def listMetadataFormats(self, identifier=None):
        """
        OAI-PMH verb, ListMetadataFormats. See
        http://www.openarchives.org/OAI/openarchivesprotocol.html#ListMetadataFormats
        """
        metadata_prefixies = [ELG, DC]
        schemata = ['http://inventory.clarin.gr/META-XMLSchema/v3.0.2/META-SHARE-Resource.xsd',
                    'https://www.dublincore.org/schemas/xmls/qdc/dc.xsd',
                    'http://www.language-archives.org/OLAC/1.1/olac.xsd',
                    'http://www.clarin.eu/cmd/resourceInfo%20corpus%20CMDI%2020130409.xsd']
        namespaces = ['http://inventory.clarin.gr/META-XMLSchema/v3.0.2',
                      'http://purl.org/dc/elements/1.1/',
                      'http://www.language-archives.org/OLAC/1.1/',
                      'http://www.clarin.eu/cmd/']
        formats = zip(metadata_prefixies, schemata, namespaces)
        return formats

    def listSets(self):
        """
        OAI-PMH verb, ListSets.
        See http://www.openarchives.org/OAI/openarchivesprotocol.html#ListSets
        """
        sets = zip(setSpec, setName, setName)
        return sets


def get_storage_objects(from_, until):
    """
    Returns StorageObject instances according to from_ and until args
    """
    storage_objects = StorageObject.objects.filter(publication_status=PUBLISHED)
    if from_:
        storage_objects = storage_objects.filter(modified__gte=from_)
    if until:
        storage_objects = storage_objects.filter(modified__lte=until)
    return storage_objects


def datestampInRange(header, from_, until):
    """
    Checks if record's datestamp is within requested dates
    """
    if (from_ is not None and header.datestamp() < from_) or \
            (until is not None and header.datestamp() > until):
        return False

    return True


def _get_setSpecs(storage_object):
    """
    the sets to which the resource belongs
    """
    from metashare.repository.models import corpusInfoType_model, \
        lexicalConceptualResourceInfoType_model, \
        languageDescriptionInfoType_model, toolServiceInfoType_model
    from metashare.repository.model_utils import get_resource_media_types

    set_ = []
    resource = storage_object.resourceinfotype_model_set.first()
    resourceComponentType = resource.resourceComponentType.as_subclass()
    resource_type = resourceComponentType.resourceType
    if isinstance(resourceComponentType, corpusInfoType_model):
        # sets are the media types, e.g text
        types = get_resource_media_types(resource)
    elif isinstance(resourceComponentType, lexicalConceptualResourceInfoType_model):
        # sets are the resource types, e.g ontology
        types = [resourceComponentType.lexicalConceptualResourceType]
    elif isinstance(resourceComponentType, languageDescriptionInfoType_model):
        # sets are the resource types, e.g grammar
        types = [resourceComponentType.languageDescriptionType]
    elif isinstance(resourceComponentType, toolServiceInfoType_model):
        # sets are the resource types, e.g platform
        types = [resourceComponentType.toolServiceType]
        # remove duplicates and preserver order
    types = orderedset(types)
    # insert resource type in the types list
    types.insert(0, resource_type)
    # create all the possible combinations between types, having as first
    # element the resource type
    combs = [resource_type]
    for i in xrange(3, len(types) + 1):
        els = [list(x) for x in combinations(types, i) if resource_type in x]
        combs += els

    for comb in combs:
        if isinstance(comb, list):
            set_.append(u":".join(comb))
        else:  # is string
            set_.append(comb)

    if exportable_to_elg(resource):
        set_.append('elg-set')

    # the source repository of the resource
    set_.insert(0, storage_object.source_url)
    return set_


def _add_elements_to_cmdi_metadata(map_, pk, pid_url):
    cmdi_xml = etree.tostring(map_['CMD'])
    cmdi_elem = etree.fromstring(cmdi_xml)
    for child in cmdi_elem:
        # TODO: The MdSelfLink is the PID and it is set in the xslt
        # If it not the PID that resolves to the cmdi metadata record
        # of the lr, uncomment the following lines and set element value
        # if child.tag.strip() == '{http://www.clarin.eu/cmd/}Header':
        #     elem = etree.SubElement(child, 'MdSelfLink')
        #     elem.text = pid_url
        if child.tag.strip() == '{http://www.clarin.eu/cmd/}Resources':
            resourceProxyList_elem = child[0]
            resourceProxy_elem = etree.SubElement(
                resourceProxyList_elem, 'ResourceProxy')
            resourceProxy_elem.attrib['id'] = 'resource-{}'.format(pk)
            resourceType_elem = etree.SubElement(
                resourceProxy_elem, 'ResourceType')
            resourceType_elem.text = 'LandingPage'
            resourceRef_elem = etree.SubElement(
                resourceProxy_elem, 'ResourceRef')
            resourceRef_elem.text = pid_url
    map_['CMD'] = cmdi_elem
    return map_


setSpec = ['corpus', 'languageDescription', 'lexicalConceptualResource',
           'toolService', 'corpus:text', 'corpus:audio', 'corpus:video',
           'corpus:textngram', 'corpus:image', 'corpus:textnumerical',
           'corpus:text:audio', 'corpus:text:video', 'corpus:text:textngram',
           'corpus:text:image', 'corpus:text:textnumerical',
           'corpus:audio:video',
           'corpus:audio:textngram', 'corpus:audio:image',
           'corpus:audio:textnumerical',
           'corpus:video:textngram', 'corpus:video:image',
           'corpus:video:textnumerical',
           'corpus:textngram:image', 'corpus:textngram:textnumerical',
           'corpus:image:textnumerical', 'corpus:text:audio:video',
           'corpus:text:audio:textngram', 'corpus:text:audio:image',
           'corpus:text:audio:textnumerical', 'corpus:text:video:textngram',
           'corpus:text:video:image', 'corpus:text:video:textnumerical',
           'corpus:text:textngram:image', 'corpus:text:textngram:textnumerical',
           'corpus:text:image:textnumerical', 'corpus:audio:video:textngram',
           'corpus:audio:video:image', 'corpus:audio:video:textnumerical',
           'corpus:audio:textngram:image',
           'corpus:audio:textngram:textnumerical',
           'corpus:audio:image:textnumerical', 'corpus:video:textngram:image',
           'corpus:video:textngram:textnumerical',
           'corpus:video:image:textnumerical',
           'corpus:textngram:image:textnumerical',
           'corpus:text:audio:video:textngram',
           'corpus:text:audio:video:image',
           'corpus:text:audio:video:textnumerical',
           'corpus:text:audio:textngram:image',
           'corpus:text:audio:textngram:textnumerical',
           'corpus:text:audio:image:textnumerical',
           'corpus:text:video:textngram:image',
           'corpus:text:video:textngram:textnumerical',
           'corpus:text:video:image:textnumerical',
           'corpus:text:textngram:image:textnumerical',
           'corpus:audio:video:textngram:image',
           'corpus:audio:video:textngram:textnumerical',
           'corpus:audio:video:image:textnumerical',
           'corpus:audio:textngram:image:textnumerical',
           'corpus:video:textngram:image:textnumerical',
           'corpus:text:audio:video:textngram:image',
           'corpus:text:audio:video:textngram:textnumerical',
           'corpus:text:audio:video:image:textnumerical',
           'corpus:text:audio:textngram:image:textnumerical',
           'corpus:text:video:textngram:image:textnumerical',
           'corpus:audio:video:textngram:image:textnumerical',
           'corpus:text:audio:video:textngram:image:textnumerical',
           'languageDescription:grammar', 'languageDescription:other',
           'lexicalConceptualResource:wordList',
           'lexicalConceptualResource:computationalLexicon',
           'lexicalConceptualResource:ontology',
           'lexicalConceptualResource:wordnet',
           'lexicalConceptualResource:thesaurus',
           'lexicalConceptualResource:framenet',
           'lexicalConceptualResource:terminologicalResource',
           'lexicalConceptualResource:machineReadableDictionary',
           'lexicalConceptualResource:lexicon',
           'lexicalConceptualResource:other',
           'toolService:tool', 'toolService:service',
           'toolService:platform', 'toolService:suiteOfTools',
           'toolService:infrastructure', 'toolService:architecture',
           'toolService:nlpDevelopmentEnvironment', 'toolService:other',
           'elg-set'
           ]
setName = ['Corpus', 'Language Description', 'Lexical Conceptual Resource',
           'Tools/Service',
           'Corpus of text media type', 'Corpus of audio media type',
           'Corpus of video media type', 'Corpus of textNGram media type',
           'Corpus of image media type', 'Corpus of textNumerical media type',
           'Corpus of media type: text, audio',
           'Corpus of media type: text, video',
           'Corpus of media type: text, textNgram',
           'Corpus of media type: text, image',
           'Corpus of media type: text, textNumerical',
           'Corpus of media type: audio, video',
           'Corpus of media type: audio, textNgram',
           'Corpus of media type: audio, image',
           'Corpus of media type: audio, textNumerical',
           'Corpus of media type: video, textNgram',
           'Corpus of media type: video, image',
           'Corpus of media type: video, textNumerical',
           'Corpus of media type: textNgram, image',
           'Corpus of media type: textNgram, textNumerical',
           'Corpus of media type: image, textNumerical',
           'Corpus of media type: text, audio, video',
           'Corpus of media type: text, audio, textNgram',
           'Corpus of media type: text, audio, image',
           'Corpus of media type: text, audio, textNumerical',
           'Corpus of media type: text, video, textNgram',
           'Corpus of media type: text, video, image',
           'Corpus of media type: text, video, textNumerical',
           'Corpus of media type: text, textNgram, image',
           'Corpus of media type: text, textNgram, textNumerical',
           'Corpus of media type: text, image, textNumerical',
           'Corpus of media type: audio, video, textNgram ',
           'Corpus of media type: audio, video, image',
           'Corpus of media type: audio, video, textNumerical',
           'Corpus of media type: audio, textNgram,  image',
           'Corpus of media type: audio, textNgram, textNumerical',
           'Corpus of media type: audio, image, textNumerical',
           'Corpus of media type: video, textNgram, image',
           'Corpus of media type: video, textNgram, textNumerical',
           'Corpus of media type: video, image, textNumerical',
           'Corpus of media type: textNgram, image, textNumerical',
           'Corpus of media type: text, audio, video, textNgram',
           'Corpus of media type: text, audio, video, image',
           'Corpus of media type: text, audio, video, textNumerical',
           'Corpus of media type: text, audio, textNgram, image',
           'Corpus of media type: text, audio, textNgram, textNumerical',
           'Corpus of media type: text, audio, image, textNumerical',
           'Corpus of media type: text, video, textNgram, image',
           'Corpus of media type: text, video, textNgram, textNumerical',
           'Corpus of media type: text, video, image, textNumerical',
           'Corpus of media type: text, textNgram, image, textNumerical',
           'Corpus of media type: audio, video, textNgram, image',
           'Corpus of media type: audio, video, textNgram, textNumerical',
           'Corpus of media type: audio, video, image, textNumerical',
           'Corpus of media type: audio, textNgram, image, textNumerical',
           'Corpus of media type: video, textNgram, image, textNumerical',
           'Corpus of media type: text, audio, video, textNgram, image',
           'Corpus of media type: text, audio, video, textNgram, textNumerical',
           'Corpus of media type: text, audio, video, image, textNumerical',
           'Corpus of media type: text, audio, textNgram, image, textNumerical',
           'Corpus of media type: text, video, textNgram, image, textNumerical',
           'Corpus of media type: audio, video, textNgram, image, textNumerical',
           'Corpus of media type: text, audio, video, textNgram, image, textNumerical',
           'Grammar', 'Other than grammar language description type',
           'Word list', 'computational lexicon', 'Ontology', 'Wordnet',
           'Thesaurus', 'Framenet', 'Terminological resource',
           'Machine readable dictionary', 'Lexicon',
           'Other lexical conceptual resource', 'Tool', 'Service', 'Platform',
           'Suite of tools', 'Infrastructure', 'Architecture',
           'Nlp development enviroment',
           'Other service or tool', 'ELG Subset']
