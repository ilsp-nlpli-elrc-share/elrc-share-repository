# coding=utf-8
# pylint:
from django.conf import settings
from oaipmh.metadata import MetadataRegistry
from oaipmh.server import Server

from metashare.oai.metadata_handlers import (
    DCWriter, ELGWriter
)
from metashare.oai.server import (
    OaiPmhServer, DC, ELG
)


def metadata_registry():
    registry = MetadataRegistry()
    registry.registerWriter(DC, DCWriter())
    registry.registerWriter(ELG, ELGWriter())
    # registry.registerWriter(METASHARE, MetashareWriter())
    # registry.registerWriter(OLAC, OlacWriter())
    # registry.registerWriter(CMDI, CmdiWriter())
    # registry.registerWriter(EDM, EDMWriter())
    return registry


# clarin:el OAI-PMH server instantiation
__env_dict = {
    "repositoryName": 'ELRC-SHARE',
    "baseURL": settings.OAIPMH_URL,
    "adminEmails": [admin[1] for admin in settings.ADMINS],
}

oaipmh_server = Server(
    server=OaiPmhServer(__env_dict),
    metadata_registry=metadata_registry(),
    resumption_batch_size=settings.OAIPMH_BATCH_SIZE
)