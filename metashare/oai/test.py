from metashare.oai.exporter.element_class import Element as ELG
from metashare.repository.models import resourceInfoType_model
from lxml import etree

from metashare.settings import DJANGO_URL

# r = resourceInfoType_model.objects.get(id=301)

NSMAP = {
    None: 'http://w3id.org/meta-share/meta-share/',
    "xml": "http://www.w3.org/XML/1998/namespace"
}

xml_lang = '{http://www.w3.org/XML/1998/namespace}lang'

schema_location = etree.QName("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation")
def get_xml(r):
    MetadataRecord = etree.Element('MetadataRecord',
                                   {schema_location: 'http://w3id.org/meta-share/meta-share/ ../Schema/ELG-SHARE.xsd'},
                                   nsmap=NSMAP)
    MetadataRecord.append(ELG(name='MetadataRecordIdentifier', attrs={
        'MetadataRecordIdentifierScheme': 'http://w3id.org/meta-share/meta-share/elg'}).element)
    MetadataRecord.append(
        ELG(name='metadataCreationDate', value=r.metadataInfo.metadataCreationDate.strftime("%Y-%m-%d")).element)
    # MetadataRecord.append(
    #     ELG(name='metadataLastDateUpdated', value=r.metadataInfo.metadataLastDateUpdated.strftime("%Y-%m-%d")).element)
    MetadataRecord.append(ELG(name='compliesWith', value='http://w3id.org/meta-share/meta-share/ELG-SHARE').element)

    sourceOfMetadataRecord = ELG(name='sourceOfMetadataRecord')
    repositoryName = ELG(name='repositoryName', value='ELRC-SHARE', attrs={xml_lang: 'en'})
    sourceOfMetadataRecord.element.append(repositoryName.element)
    MetadataRecord.append(sourceOfMetadataRecord.element)

    sourceMetadataRecord = ELG(name='sourceMetadataRecord')
    sourceMetadataRecord.element.append(
        ELG(name='MetadataRecordIdentifier', value='{}{}'.format(DJANGO_URL, r.get_absolute_url()), attrs={
            'MetadataRecordIdentifierScheme': 'other'}).element)
    MetadataRecord.append(sourceMetadataRecord.element)

    # DescribedEntity
    DescribedEntity = ELG(name='DescribedEntity')
    # LanguageResource
    LanguageResource = ELG(name='LanguageResource')
    LanguageResource.add_elements(
        [
            ELG(name='entityType', value='LanguageResource').element,
            [
                ELG(name='resourceName', value=v, attrs={xml_lang: k}).element for k, v in
                r.identificationInfo.resourceName.items()
            ],
            [
                ELG(name='description', value=v, attrs={xml_lang: k}).element for k, v in
                r.identificationInfo.description.items()
            ],

        ]
    )

    DescribedEntity.element.append(LanguageResource.element)

    MetadataRecord.append(DescribedEntity.element)

    return etree.tostring(MetadataRecord, pretty_print=True)

# print etree.tostring(MetadataRecord, pretty_print=True, encoding='UTF-8')
