{% extends "base.html" %}
{% load email_protection %}

{% block content %}

    <div id="general" class="anchor"></div>
    <h2>What is ELRC-SHARE</h2>
    <div class="content_box">
        <p>
            ELRC-SHARE is a <a href="http://lr-coordination.eu/resources" target="_blank">Language Resources</a> (LR)
            repository used for documenting, storing, browsing and accessing language data and tools that are pertinent
            to Automated Translation and considered useful for feeding
            <a href="http://lr-coordination.eu/discover#f3" target="_blank">CEF eTranslation</a>, the European
            Commission's Automated Translation platform.
        </p>
        <p>
            ELRC-SHARE has been developed and is maintained by the
            <a href="http://lr-coordination.eu/" target="_blank">European Language Resource Coordination (ELRC)</a>
            consortium in the framework of the ELRC action (Service contracts SMART 2014/1074, contract number
            30-CE-0696785/00-64 and SMART 2015/1091, contract number 30-CE-0816330/00-16).
        </p>
    </div>
    <div id="LR_Provision" class="anchor"></div>
    <h2>Provision of Language Resources in the ELRC-SHARE repository</h2>
    <div class="content_box">
        <p>
            The <a href="http://www.lr-coordination.eu/" target="_blank">ELRC action</a> is looking for open data that
            can be made available for re-use through open data initiatives, but also for datasets with restrictive
            licensing or made available under commercial conditions. You are welcome to upload and document any LRs
            that you have identified as useful for the purposes of CEF eTranslation and you have the license to
            contribute/deposit.
        </p>
        <p>
            To <a href="https://elrc-share.eu/repository/contribute">contribute</a> a LR to ELRC-SHARE, you need to
            <a href="https://elrc-share.eu/accounts/create/">register</a> and accept the
            <a href="https://www.elrc-share.eu/static/metashare/ELRC-SHARE_ToS.pdf" target="_blank">Terms of Service
                for registered users</a>.
        </p>
        <p>
            To <a href="https://elrc-share.eu/accounts/create/">register</a> and
            <a href="https://elrc-share.eu/repository/contribute">contribute</a> resources to ELRC-SHARE follow these
            <a href="https://www.elrc-share.eu/static/metashare/Walkthrough_for_Contributors.pdf" target="_blank">simple
                steps</a>.
        </p>
        <p>
            Once a LR is contributed to ELRC-SHARE, authorised users from the ELRC consortium ("editors") are assigned
            the task of enriching the descriptions of the resources provided by contributors according to
            the <a href="https://elrc-share.eu/documentation/elrcShareSchema.html" target="_blank">ELRC-SHARE metadata
            schema</a> and publishing them at the ELRC-SHARE inventory.
        </p>
    </div>
    <div id="access" class="anchor"></div>
    <h2>Access to ELRC-SHARE Language Resources</h2>
    <div class="content_box">
        <p>
            Anyone can browse the ELRC-SHARE inventory and view the resource descriptions. In addition, by contract
            (Service Contract SMART 2015/1091, contract number 30-CE-0816330/00-16), ELRC, through ELRC-SHARE, publishes
            metadata of openly licensed LRs on the EU Open Data Portal at
            <a href="https://data.europa.eu/euodp/en/home" target="_blank">https://data.europa.eu/euodp/en/home</a>.
        </p>
        <p>
            Anyone can download resources if the licensing terms under which they are provided, permit it
            (cf. <a href="#Licensing_LRs">below</a>). Please note that resources provided with a
            <b>"non-standard/Other_Licence/Terms"</b> licence are and will not be available for download through
            the ELRC-SHARE inventory. The rights of use of the resource, any possible restrictions, as well as
            rights and restrictions on the original raw data are under the control and responsibility
            of the data owners. The repository acts mainly as a facilitator for the search-and-get procedure
            while providing guidelines and metadata curation activities.
        </p>
        <p>
            Users are requested to always investigate the metadata elements regarding rights of use, availability and
            distribution to promptly understand what they are allowed to do with a specific resource, before using
            any of the resources in this repository.
        </p>
    </div>
    <div id="Licensing_LRs" class="anchor"></div>
    <h2>Licensing LRs for the ELRC action</h2>
    <div class="content_box">
        <p>
            Any grant of access to LRs through the ELRC-SHARE repository should include not only the right to read the
            relevant content but also to allow transformative uses, dissemination and distribution of such resources
            and their derivatives, according to the needs and policies of LR owners and users.
        </p>
        <p>
            To limit the complexity of licensing, we strongly advise LR providers to use one of the standard licences,
            i.e. Creative Commons and open national licences (- cf. <a href="#Licences">table</a>
            below). If, however, the LR comes with another licence, the licensing terms and conditions must be
            documented in the metadata so that they are clearly available to all repository users. Please note that
            resources provided with a <b>"non-standard/Other_Licence/Terms"</b> licence are and will not be available
            for
            download through the ELRC-SHARE inventory.
        </p>
        <p>
            A <a href="http://www.lr-coordination.eu/helpdesk" target="_blank">helpdesk facility</a> has been set up
            for assistance in understanding, choosing and using any of these licences and other legal tools, as well
            as the metadata required for the documentation of the licence.
        </p>
        <div id="Standard_and_National_Open_Licences" class="anchor"></div>
        <h3>Licences</h3>
        <table id="Licences">
            <thead>
            <tr>
                <th>Value</th>
                <th>Full name with link to licence text or Definition</th>
            </tr>
            </thead>
            <tbody>
            <!-- International/National Data Licences -->
            <tr>
                <td colspan="2">
                    <div style="font-size:1.2em; color: #4d4d4d;;">International/National Data Licences</div>
                    <hr/>
                </td>
            </tr>
            <tr>
                <td>CC0-1.0</td>
                <td><a href="https://creativecommons.org/publicdomain/zero/1.0/"
                       target="_blank">Creative Commons Zero</a></td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-4.0</td>
                <td><a href="https://creativecommons.org/licenses/by/4.0/"
                       target="_blank">Creative Commons Attribution v4.0</a></td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-NC-4.0</td>
                <td><a href="https://creativecommons.org/licenses/by-nc/4.0/"
                       target="_blank">Creative Commons Attribution-NonCommercial v4.0</a></td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-NC-ND-4.0</td>
                <td><a href="https://creativecommons.org/licenses/by-nc-nd/4.0/"
                       target="_blank">Creative Commons Attribution- NonCommercial-NoDerivatives v4.0</a></td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-NC-SA-4.0</td>
                <td><a href="https://creativecommons.org/licenses/by-nc-sa/4.0/"
                       target="_blank">Creative Commons Attribution-NonCommercial-ShareAlike v4.0</a></td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-ND-4.0</td>
                <td><a href="https://creativecommons.org/licenses/by-nd/4.0/"
                       target="_blank">Creative Commons Attribution-NoDerivatives v4.0</a></td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-SA-4.0</td>
                <td><a href="https://creativecommons.org/licenses/by-sa/4.0/"
                       target="_blank">Creative Commons Attribution-ShareAlike v4.0</a></td>
                </td>
            </tr>
            <tr>
                <td>ODbL-1.0</td>
                <td><a href="http://www.opendatacommons.org/licenses/odbl/1.0/"
                       target="_blank">ODC Open Database License v1.0</a></td>
                </td>
            </tr>
            <tr>
                <td>ODC-BY-1.0</td>
                <td><a href="http://opendatacommons.org/licenses/by/" target="_blank">Open Data Commons Attribution
                    License</a></td>
                </td>
            </tr>
            <tr>
                <td>PDDL-1.0</td>
                <td><a href="https://opendatacommons.org/licenses/pddl/1.0/"
                       target="_blank">ODC Public Domain Dedication & License 1.0</a></td>
                </td>
            </tr>
            <tr>
                <td>openUnder_PSI</td>
                <td>Used for resources that are PSI-compliant and no further information is required or available
                </td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-3.0</td>
                <td><a href="https://creativecommons.org/licenses/by/3.0/"
                       target="_blank">Creative Commons Attribution v3.0</a></td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-NC-3.0</td>
                <td><a href="https://creativecommons.org/licenses/by-nc/3.0/"
                       target="_blank">Creative Commons Attribution-NonCommercial v3.0</a></td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-NC-ND-3.0</td>
                <td><a href="https://creativecommons.org/licenses/by-nc-nd/3.0/"
                       target="_blank">Creative Commons Attribution- NonCommercial-NoDerivatives v3.0</a></td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-NC-SA-3.0</td>
                <td><a href="https://creativecommons.org/licenses/by-nc-sa/3.0/"
                       target="_blank">Creative Commons Attribution-NonCommercial-ShareAlike v3.0</a></td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-ND-3.0</td>
                <td><a href="https://creativecommons.org/licenses/by-nd/3.0/"
                       target="_blank">Creative Commons Attribution-NoDerivatives v3.0</a></td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-SA-3.0</td>
                <td><a href="https://creativecommons.org/licenses/by-sa/3.0/"
                       target="_blank">Creative Commons Attribution-ShareAlike v3.0</a></td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-2.0</td>
                <td><a href="https://creativecommons.org/licenses/by/2.0/"
                       target="_blank">Creative Commons Attribution v2.0</a></td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-NC-2.0</td>
                <td><a href="https://creativecommons.org/licenses/by-nc/2.0/"
                       target="_blank">Creative Commons Attribution-NonCommercial v2.0</a></td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-NC-ND-2.0</td>
                <td><a href="https://creativecommons.org/licenses/by-nc-nd/2.0/"
                       target="_blank">Creative Commons Attribution- NonCommercial-NoDerivatives v2.0</a></td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-NC-SA-2.0</td>
                <td><a href="https://creativecommons.org/licenses/by-nc-sa/2.0/"
                       target="_blank">Creative Commons Attribution-NonCommercial-ShareAlike v2.0</a></td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-ND-2.0</td>
                <td><a href="https://creativecommons.org/licenses/by-nd/2.0/"
                       target="_blank">Creative Commons Attribution-NoDerivatives v2.0</a></td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-SA-2.0</td>
                <td><a href="https://creativecommons.org/licenses/by-sa/2.0/"
                       target="_blank">Creative Commons Attribution-ShareAlike v2.0</a></td>
                </td>
            </tr>
            <!-- National Data Licences -->
            <tr>
                <td colspan="2">
                    <div style="font-size:1.2em; color: #4d4d4d;;">National Data Licences</div>
                    <hr/>
                </td>
            </tr>
            <tr>
                <td>dl-de/by-2-0</td>
                <td><a href="https://www.govdata.de/dl-de/by-2-0"
                       target="_blank">Datenlizenz Deutschland – Namensnennung – Version 2.0</a> (Germany)
                </td>
                </td>
            </tr>
            <tr>
                <td>dl-de/zero-2-0</td>
                <td><a href="https://www.govdata.de/dl-de/zero-2-0"
                       target="_blank">Datenlizenz Deutschland – Zero – Version 2.0</a> (Germany)
                </td>
                </td>
            </tr>
            <tr>
                <td>IODL-1.0</td>
                <td><a href="http://www.formez.it/iodl/" target="_blank">Italian Open Data License v1.0</a> (Italy)
                </td>
                </td>
            </tr>
            <tr>
                <td>LO-OL-v2</td>
                <td><a href="https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf"
                       target="_blank">Licence
                    Ouverte / Open Licence v2.0</a> (France)
                </td>
                </td>
            </tr>
            <tr>
                <td>NCGL-1.0</td>
                <td>
                    <a href="http://www.nationalarchives.gov.uk/doc/non-commercial-government-licence/non-commercial-government-licence.htm"
                       target="_blank">Non-Commercial Government Licence</a> (UK)
                </td>
                </td>
            </tr>
            <tr>
                <td>NLOD-1.0</td>
                <td><a href="http://data.norge.no/nlod/en/1.0"
                       target="_blank">Norwegian Licence for Open Government Data</a> (Norway)
                </td>
                </td>
            </tr>
            <tr>
                <td>OGL-UK-3.0</td>
                <td><a href="http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/"
                       target="_blank">Open Government Licence 3.0</a> (UK)
                </td>
                </td>
            </tr>
            <tr>
                <td>OGL-UK-2.0</td>
                <td><a href="http://www.nationalarchives.gov.uk/doc/open-government-licence/version/2/"
                       target="_blank">Open Government Licence 2.0</a> (UK)
                </td>
                </td>
            </tr>
            <tr>
                <td>OGL-UK-1.0</td>
                <td><a href="http://www.nationalarchives.gov.uk/doc/open-government-licence/version/1/"
                       target="_blank">Open Government Licence 1.0</a> (UK)
                </td>
                </td>
            </tr>
            <tr>
                <td>OGL-CA-2.0</td>
                <td><a href="https://open.canada.ca/en/open-government-licence-canada"
                       target="_blank">Open Government Licence 2.0</a> (Canada)
                </td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-3.0-AT</td>
                <td><a href="https://creativecommons.org/licenses/by/3.0/cz/deed.en"
                       target="_blank">Creative Commons Attribution 3.0</a> (Austria)
                </td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-3.0-CZ</td>
                <td><a href="https://creativecommons.org/licenses/by/3.0/cz/deed.en"
                       target="_blank">Creative Commons Attribution 3.0</a> (Czech Republic)
                </td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-3.0-IT</td>
                <td><a href="https://creativecommons.org/licenses/by/3.0/it/deed.en"
                       target="_blank">Creative Commons Attribution 3.0</a> (Italy)
                </td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-SA-2.5_Sl</td>
                <td><a href="https://creativecommons.org/licenses/by-sa/2.5/si/deed.en"
                       target="_blank">Creative Commons Attribution-ShareAlike 2.5</a> (Slovenia)
                </td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-SA-3.0-PL</td>
                <td><a href="https://creativecommons.org/licenses/by-sa/3.0/pl/"
                       target="_blank">Creative Commons Attribution-ShareAlike 3.0</a> (Poland)
                </td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-NC-SA-3.0-PL</td>
                <td><a href="https://creativecommons.org/licenses/by-nc-sa/3.0/pl/"
                       target="_blank">Creative Commons Attribution Non Commercial Share Alike 3.0</a> (Poland)
                </td>
                </td>
            </tr>
            <tr>
                <td>CC-BY-2.5-SE</td>
                <td><a href="https://creativecommons.org/licenses/by/2.5/se/deed.en"
                       target="_blank">Creative Commons Attribution 2.5</a> (Sweden)
                </td>
                </td>
            </tr>
            <!-- International Software Licences -->
            <tr>
                <td colspan="2">
                    <div style="font-size:1.2em; color: #4d4d4d;;">International Software Licences</div>
                    <hr/>
                </td>
            </tr>
            <tr>
                <td>AGPL-3.0</td>
                <td><a href="http://www.opensource.org/licenses/AGPL-3.0"
                       target="_blank">GNU Affero General Public License v3.0</a></td>
                </td>
            </tr>
            <tr>
                <td>Apache-2.0</td>
                <td><a href="http://www.apache.org/licenses/LICENSE-2.0"
                       target="_blank">Apache License 2.0</a></td>
                </td>
            </tr>
            <tr>
                <td>BSD-2-Clause</td>
                <td><a href="http://www.opensource.org/licenses/BSD-2-Clause"
                       target="_blank">BSD 2-clause "Simplified" License</a></td>
                </td>
            </tr>
            <tr>
                <td>BSD-3-Clause</td>
                <td><a href="http://www.opensource.org/licenses/BSD-3-Clause"
                       target="_blank">BSD 3-clause "New" or "Revised" License</a></td>
                </td>
            </tr>
            <tr>
                <td>BSD-4-Clause</td>
                <td><a href="https://spdx.org/licenses/BSD-4-Clause.html"
                       target="_blank">BSD 4-clause "Original" or "Old" License</a></td>
                </td>
            </tr>
            <tr>
                <td>EPL-1.0</td>
                <td><a href="http://www.eclipse.org/legal/epl-v10.html"
                       target="_blank">Eclipse Public License 1.0</a></td>
                </td>
            </tr>
            <tr>
                <td>GFDL-1.3</td>
                <td><a href="http://www.gnu.org/licenses/fdl-1.3.txt"
                       target="_blank">GNU Free Documentation License v1.3</a></td>
                </td>
            </tr>
            <tr>
                <td>GPL-2.0</td>
                <td><a href="http://www.gnu.org/licenses/gpl-2.0-standalone.html"
                       target="_blank">GNU General Public License v2.0</a></td>
                </td>
            </tr>
            <tr>
                <td>GPL-3.0</td>
                <td><a href="http://www.gnu.org/licenses/gpl-3.0-standalone.html"
                       target="_blank">GNU General Public License v3.0</a></td>
                </td>
            </tr>
            <tr>
                <td>LGPL-2.1</td>
                <td><a href="http://www.gnu.org/licenses/lgpl-2.1-standalone.html"
                       target="_blank">GNU Lesser General Public License v2.1</a></td>
                </td>
            </tr>
            <tr>
                <td>LGPL-3.0</td>
                <td><a href="http://www.gnu.org/licenses/lgpl-3.0-standalone.html"
                       target="_blank">GNU Lesser General Public License v3.0 only</a></td>
                </td>
            </tr>
            <tr>
                <td>MIT</td>
                <td><a href="http://www.opensource.org/licenses/MIT"
                       target="_blank">MIT License</a></td>
                </td>
            </tr>
            <tr>
                <td>MPL-2.0</td>
                <td><a href="http://www.mozilla.org/en-US/MPL/2.0/"
                       target="_blank">Mozilla Public License</a></td>
                </td>
            </tr>
            <!-- International Software & Data Licences -->
            <tr>
                <td colspan="2">
                    <div style="font-size:1.2em; color: #4d4d4d;;">International Software & Data Licences</div>
                    <hr/>
                </td>
            </tr>
            <tr>
                <td>EUPL-1.0</td>
                <td><a href="http://ec.europa.eu/idabc/servlets/Doc027f.pdf?id=31096"
                       target="_blank">European Union Public Licence 1.0</a></td>
                </td>
            </tr>
            <tr>
                <td>EUPL-1.1</td>
                <td>
                    <a href="https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl1.1.-licence-en_0.pdf"
                       target="_blank">European Union Public Licence 1.1</a></td>
                </td>
            </tr>
            <tr>
                <td>EUPL-1.2</td>
                <td>
                    <a href="https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl_v1.2_en.pdf"
                       target="_blank">European Union Public Licence 1.2</a></td>
                </td>
            </tr>
            <!-- Other -->
            <tr>
                <td colspan="2">
                    <div style="font-size:1.2em; color: #4d4d4d;;">Other</div>
                    <hr/>
                </td>
            </tr>
            <tr>
                <td>publicDomain</td>
                <td>Used for resources that are free of all known legal restrictions
                </td>
                </td>
            </tr>
            <tr>
                <td>non-standard/Other_licence/Terms</td>
                <td>Resources with this value are distributed with a non-standard or proprietary licence. Resource
                    providers are prompted to provide the licence text or url of the resource for further details
                    about the restrictions and/or conditions of use and other obligations.
                </td>
                </td>
            </tr>
            <tr>
                <td>underReview</td>
                <td>Used for resources whose licence value is pending</td>
                </td>
            </tr>
            <tbody>
        </table>
    </div>
    <div id="Notice_and_Take_Down_Policy" class="anchor"></div>
    <h2>Notice and Take Down Policy</h2>
    <div class="content_box">
        <p>
            If a rights holder is concerned that s/he has found Language Resources (LRs) on the ELRC-SHARE
            repository, for which s/he has not given permission, granted a licence or is not covered by a limitation
            or exception in national law, such rights holder is asked to contact ELRC-SHARE
            ({% encrypt_email 'elrc-share@ilsp.gr' %}) in writing, stating the following:
        </p>
        <ol>
            <li>His/Her contact details (name, organisation, email, telephone number);</li>
            <li>The full bibliographic details of the LR;</li>
            <li>The exact and full URL where s/he found the LR;</li>
            <li>Proof that s/he is the rights holder and a statement that, under penalty of perjury, s/he is the
                rights holder or an authorised representative.
            </li>
        </ol>
        <p>Upon receipt of notification the <b>Notice and Takedown</b> procedure is then invoked as follows:</p>
        <ol>
            <li>
                ELRC-SHARE will acknowledge receipt of the complaint by email or letter and will make an initial
                assessment of its validity and plausibility.
            </li>
            <li>
                Upon receipt of a valid complaint, the LR will be temporarily removed from the ELRC-SHARE inventory
                pending an agreed solution.
            </li>
            <li>
                ELRC-SHARE will contact the individual or organisation who deposited the material within ten working
                days from the reception of a valid complaint. The ELRC-SHARE depositor will be notified that the
                material is subject to a complaint, under what allegations, and will be encouraged to assuage the
                complaints concerned.
            </li>
            <li>
                The complainant and the ELRC-SHARE depositor will be encouraged to resolve the issue swiftly and
                amicably and to the satisfaction of both parties, with the following possible outcomes:
                <ul>
                    <li>The LR is replaced on the ELRC-SHARE inventory unchanged.</li>
                    <li>The LR is replaced on the ELRC-SHARE inventory with changes.</li>
                    <li>The LR is permanently removed from the ELRC-SHARE inventory.</li>
                </ul>
            </li>
            <li>
                If the ELRC-SHARE depositor and the complainant are unable to agree on a solution, the LR will
                remain unavailable through the ELRC-SHARE inventory until a time when a resolution has been reached.
            </li>
        </ol>
    </div>
    <div id="disclaimers" class="anchor"></div>
    <h2>Disclaimers and Limitation of Liability</h2>
    <div class="content_box">
        <p>
            ELRC-SHARE content and metadata are provided “AS IS” and on an “ΑS AVAILABLE” basis without any
            representations or any kind of warranty made (whether express or implied by law) to the extent permitted
            by law, including the implied warranties of satisfactory quality, fitness for a particular purpose,
            non-infringement, compatibility, security and accuracy.
        </p>
        <p>
            Under no circumstances will ELRC-SHARE be liable for any of the following losses or damage (whether such
            losses where foreseen, foreseeable, known or otherwise): (a) loss of data; (b) loss of opportunity; (c)
            loss of goodwill or injury to reputation; (d) losses suffered by third parties; or (e) any indirect,
            consequential, special or exemplary damages arising from the use of the ELRC-SHARE Services regardless
            of the form of action.
        </p>
        <p>
            ELRC-SHARE does not warrant that functions contained in the ELRC-SHARE Services will be uninterrupted or
            error free, that defects will be corrected, or that the ELRC-SHARE databases or the delivery mechanism
            that make it available are free of viruses and bugs.
        </p>
    </div>
    <div id="cookies" class="anchor"></div>
    <h2>Log information, cookies and analytics</h2>
    <div class="content_box">
        <p>
            The ELRC-SHARE website and services log certain information about every request sent to them. This
            information is used for system administration and for statistical purposes. Summary statistics are
            extracted from this data and some of these may be made publicly available, but these do not include
            information from which individuals could be identified. Relevant subsets of this data may be used as part
            of investigations of computer misuse involving this site. Data may also on occasion be used to enable
            investigation of technical problems on the website. Otherwise logged information is not passed to any
            third party except if required by law.
        </p>
        <p>
            We may also collect identification data of users, by using cookies.
        </p>
        <p>
            Cookies are short software code texts, which are sent from Athena RC server and are stored at your
            terminal ("browser"). Their basic function is to communicate to us data from your browser. Cookies are
            either "Temporary" (session cookies) or persistent. Temporary cookies are automatically deleted when you
            close your browser, while persistent cookies remain stored in your terminal until they expire.
        </p>
        <p>
            The use of cookies helps the website to remember information about the user’s visit, such as your preferred
            language and your preferences, to make secure searches, to calculate the number of visitors, or to
            facilitate your registration in our services. Cookies do not have access to data on your hard disk or
            cookies created by other sites and do not damage your system. You may control and/or delete cookies as you
            wish. You may find more details at <a href="https://aboutcookies.org/" target="_blank">aboutcookies.org</a>.
        </p>
        <p>
            You may delete all cookies from your computer and, in most browsers, select settings that do not allow
            installation of cookies. However, in that case, you may have to adjust certain preferences every time you
            visit a website. Users may make use of the website without problems even without use of cookies, but,
            potentially, to the detriment of its user-friendliness and of the functioning of certain of its services.
            Any personal data collected via cookies that can be correlated to a specific visitor/user shall be
            processed exclusively for the purposes cited above.
        </p>
        <p>
            This website uses ELRC-SHARE Analytics, a local self-hosted installation of the open source “Matomo” web
            analytics software.
        </p>
        <p>
            ELRC-SHARE Analytics uses "cookies", which are text files placed on Your computer, to help the website
            analyze how
            users use the site. The information generated by the cookie about Your use of the website is stored locally
            in Athena RC servers and it is not transmitted to any third party.
        </p>
        <p>
            In case IP-anonymisation is activated on the ELRC-SHARE website, Your IP address will be truncated within
            the area of Member States of the European Union or other parties to the Agreement on the European Economic
            Area. The IP--anonymisation is active on this website. The IP-address, that Your browser conveys within the
            scope of ELRC-SHARE Analytics, will not be associated with any other data.
        </p>
        <p>
            ELRC-SHARE Analytics will use this information on behalf of the operator of this website for the purpose
            of evaluating Your use of the website, compiling reports on website activity for website operators and
            providing them other services relating to website activity and internet usage.
        </p>
        <p>
            You may deactivate or restrict the transmission of cookies by changing the settings of your web browser.
            Cookies that are already stored may be deleted at any time.
        </p>
    </div>
    <div id="dpr" class="anchor"></div>
    <h2>Data Protection Record</h2>
    <div class="content_box">
        <p>
            The Data Protection Record for the ELRC-SHARE repository is available
            <a href="{{ STATIC_URL }}metashare/DataProtection_Repository.pdf" target="_blank">here</a>.
        </p>
    </div>
{% endblock %}
