from django.contrib.admin import FieldListFilter
from django.contrib.admin import SimpleListFilter
from django.utils.translation import ugettext as _

from .models import DELIVERABLES


class PublicationStatusFilter(SimpleListFilter):
    title = 'Publication Status'
    parameter_name = "publication_status"

    def lookups(self, request, model_admin):
        """
            List of values to allow admin to select
        """
        return (
            ('internal', 'internal'),
            ('ingested', 'ingested'),
            ('published', 'published')
        )

    def queryset(self, request, queryset):
        status = dict(internal='i', ingested='g', published='p')

        if self.value():
            try:
                return queryset.filter(resource__storage_object__publication_status=status.get(self.value()))
            except KeyError:
                return queryset
        else:
            return queryset


class DeliveredFilter(SimpleListFilter):
    title = 'Delivered'
    parameter_name = "delivered_to_EC"

    def lookups(self, request, model_admin):
        """
            List of values to allow admin to select
        """
        delivs = [d for d in DELIVERABLES['choices']]
        delivs.append(('None', 'None'))
        return tuple(delivs)

    def queryset(self, request, queryset):
        if self.value():
            if self.value() != 'None':
                return queryset.filter(delivered_to_EC=self.value())
            elif self.value() == "None":
                return queryset.filter(delivered_to_EC=None)
        else:
            return queryset


class ToBeDeliveredFilter(SimpleListFilter):
    title = 'To be Delivered to EC'
    parameter_name = "to_be_delivered_to_EC"

    def lookups(self, request, model_admin):
        """
            List of values to allow admin to select
        """
        delivs = [d for d in DELIVERABLES['choices']]
        delivs.append(('None', 'None'))
        return tuple(delivs)

    def queryset(self, request, queryset):
        if self.value():
            if self.value() != 'None':
                return queryset.filter(to_be_delivered_to_EC=self.value())
            elif self.value() == "None":
                return queryset.filter(to_be_delivered_to_EC=None)
        else:
            return queryset


class ActiveValueFilter(FieldListFilter):
    """list_filter which displays only the values for a field which are in use

    This is handy when a large range has been filtered in some way which means
    most of the potential values aren't currently in use. This requires a
    queryset field or annotation named "item_count" to be present so it can
    simply exclude anything where item_count=0.
    Usage::
    class MyAdmin(ModelAdmin):
        list_filter = (('title', ActiveValueFilter))
    """

    def __init__(self, field, request, params, model, model_admin, field_path):
        self.field_path = field_path
        self.lookup_kwarg = field_path
        self.lookup_val = request.GET.get(self.lookup_kwarg, None)
        super(ActiveValueFilter, self).__init__(field, request, params,
                                                model, model_admin,
                                                field_path)

        qs = model_admin.get_queryset(request)

        qs = qs.exclude(item_count=0)

        qs = qs.order_by(field.name).values_list(field.name, flat=True)

        self.active_values = qs.distinct()

    def expected_parameters(self):
        return [self.lookup_kwarg]

    def choices(self, cl):
        yield {
            'selected': self.lookup_val is None,
            'query_string': cl.get_query_string({}, [self.lookup_kwarg]),
            'display': _('All')
        }
        split_values = []
        for v in self.active_values:
            split_values.extend(v.split(', '))
        for lookup in set(split_values):
            if lookup != '':
                self.lookup_kwarg = '{}__{}'.format(self.field_path, 'contains')
            else:
                self.lookup_kwarg = '{}__{}'.format(self.field_path, 'exact')
            yield {
                'selected': lookup == self.lookup_val,
                'query_string': cl.get_query_string({
                    self.lookup_kwarg: lookup}, remove=[u'projects']),
                'display': lookup if lookup != '' else 'None',
            }
