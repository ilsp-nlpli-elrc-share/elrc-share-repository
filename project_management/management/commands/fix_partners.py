import logging
from django.core.management import BaseCommand
from project_management.models import ManagementObject, FUNDING_PROJECTS
from metashare.repository.fields import best_lang_value_retriever
from django.db.models import Q

LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):
    def handle(self, *args, **options):
        LOGGER.info("Fixing resource partners")
        mo = ManagementObject.objects.filter(Q(partner_responsible='') | Q(partner_responsible=None))
        for m in mo:
            try:
                if not m.partner_responsible and (m.resource.contactPerson.first().communicationInfo.country != None
                                                  and m.resource.contactPerson.first().communicationInfo.country):
                    projects = [best_lang_value_retriever(p.projectShortName) for p in
                                m.resource.resourceCreationInfo.fundingProject.all()]
                    if set(FUNDING_PROJECTS).intersection(projects):
                        LOGGER.info('Fixing partner for resource {}'.format(m.resource.id))
                        m.resource.save()
            except AttributeError:
                pass
