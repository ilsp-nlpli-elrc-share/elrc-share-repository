ELRC-SHARE SOFTWARE
===================

If you use ELRC-SHARE in your work, please cite: 

Piperidis, S., Labropoulou, P., Deligiannis, M., and Giagkou, M. (2018). 
[Managing public sector data for 
multilingual applications development](https://www.aclweb.org/anthology/L18-1205). 
In Proceedings of the 11th Language 
Resources and Evaluation Conference, Miyazaki, Japan. ([BibTex](https://www.aclweb.org/anthology/L18-1205.bib))