﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:olac="http://www.language-archives.org/OLAC/1.1/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:ms="https://inventory.clarin.gr/META-XMLSchema/v3.0.2" xmlns:xalan="http://xml.apache.org/xalan" exclude-result-prefixes="ms xalan">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" xalan:indent-amount="4"/>
	<!-- examples:

http://www.language-archives.org/cgi-bin/olaca3.pl?verb=GetRecord&identifier=oai:crdo.vjf.cnrs.fr:cocoon-687e85c5-f3e6-39a4-a807-d885833dbca7&metadataPrefix=olac
		http://www.language-archives.org/cgi-bin/olaca3.pl?verb=GetRecord&identifier=oai:catalogue.elra.info:ELRA-S0282-02&metadataPrefix=olac
		http://www.language-archives.org/cgi-bin/olaca3.pl?verb=GetRecord&identifier=oai:clarin.eurac.edu:20.500.12124/10&metadataPrefix=olac-->
	<!-- order of elements https://www.dublincore.org/schemas/xmls/qdc/dcterms.xsd -->
	<!-- NOT FOR NOW map other dates (dcterms:issued, dcterms:modified, dc:date) -->
	<!-- NOT FOR NOW map ms:conformanceToStandardsBestPractices to dcterms:conformsTo  -->
	<!-- NOT FOR NOW if we want to create rules for ddc values -->

	<xsl:variable name="specialRelationTypes" select="'containsSubsetOf hasPart isSuperSetOf hasAlignedVersion isAlignedVersionOf isAnnotationOf isProcessedVersionOf isPartOf isSubsetOf'" />
	<xsl:variable name="isVersionOfTypes" select="'isAlignedVersionOf isAnnotationOf isProcessedVersionOf'" />
	<xsl:variable name="isPartOfTypes" select="'isPartOf isSubsetOf'" />
	<xsl:variable name="hasPartTypes" select="'containsSubsetOf hasPart isSuperSetOf'" />

	<xsl:template match="ms:resourceComponentType//ms:resourceType">
		<xsl:if test="not(parent::ms:inputInfo) and not(parent::ms:outputInfo)">
			<xsl:choose>
				<xsl:when test="*|node()='corpus'">
					<dc:type xsi:type="olac:linguistic-type" olac:code="primary_text"/>
				</xsl:when>
				<xsl:when test="*|node()='lexicalConceptualResource'">
					<dc:type xsi:type="olac:linguistic-type" olac:code="lexicon"/>
				</xsl:when>
				<xsl:when test="*|node()='languageDescription'">
					<dc:type xsi:type="olac:linguistic-type" olac:code="language_description"/>
				</xsl:when>
				<xsl:when test="*|node()='toolService'">
					<dc:type xsi:type="dcterms:DCMIType">
						<xsl:text>Software</xsl:text>
					</dc:type>
				</xsl:when>
			</xsl:choose>
			<dc:type>
				<xsl:value-of select="."/>
			</dc:type>
		</xsl:if>
	</xsl:template>
	<xsl:template match="//ms:mediaType">
		<xsl:if test="not(parent::ms:inputInfo) and not(parent::ms:outputInfo)">
			<dc:type xsi:type="dcterms:DCMIType">
				<xsl:choose>
					<xsl:when test="*|node()='text' or *|node()='textNgram' or *|node()='textNumerical'">
						<xsl:text>Text</xsl:text>
					</xsl:when>
					<xsl:when test="*|node()='audio'">
						<xsl:text>Sound</xsl:text>
					</xsl:when>
					<xsl:when test="*|node()='video'">
						<xsl:text>MovingImage</xsl:text>
					</xsl:when>
					<xsl:when test="*|node()='image'">
						<xsl:text>Image</xsl:text>
					</xsl:when>
				</xsl:choose>
			</dc:type>
		</xsl:if>
	</xsl:template>
	<xsl:template match="ms:resourceInfo">
		<xsl:text>
	</xsl:text>
		<olac:olac>
			<xsl:attribute name="xsi:schemaLocation"><xsl:text>http://www.language-archives.org/OLAC/1.1/ http://www.language-archives.org/OLAC/1.1/olac.xsd</xsl:text></xsl:attribute>
			<!--		dc:title		-->
			<xsl:for-each select="./ms:identificationInfo/ms:resourceName">
				<dc:title>
					<xsl:attribute name="xml:lang"><xsl:value-of select="@lang"/></xsl:attribute>
					<xsl:value-of select="(.)"/>
				</dc:title>
			</xsl:for-each>
			<!--		END OF dc:title		-->
			<!--		dc:creator		-->
			<xsl:for-each select="ms:resourceCreationInfo/ms:resourceCreator">
				<dc:creator>
					<xsl:if test="ms:personInfo">
						<xsl:value-of select="concat(ms:personInfo/ms:surname[@lang='en'], ' ', ms:personInfo/ms:givenName[@lang='en'])"/>
						<xsl:if test="ms:personInfo/ms:affiliation/ms:organizationName[@lang='en']">
							<xsl:text>, </xsl:text>
							<xsl:value-of select="ms:personInfo/ms:affiliation/ms:organizationName[@lang='en']"/>
						</xsl:if>
					</xsl:if>
					<xsl:if test="ms:organizationInfo/ms:organizationName[@lang='en']">
						<xsl:value-of select="ms:organizationInfo/ms:organizationName[@lang='en']"/>
					</xsl:if>
				</dc:creator>
			</xsl:for-each>
			<!--		END OF dc:creator		-->
			<!-- 		dc:subject 		-->
			<xsl:for-each select="//ms:subject_topic">
				<dc:subject>
					<xsl:value-of select="(.)"/>
				</dc:subject>
			</xsl:for-each>
			<xsl:for-each select="//ms:domain">
				<dc:subject>
					<xsl:value-of select="(.)"/>
				</dc:subject>
			</xsl:for-each>
			<xsl:if test="//ms:toolServiceSubtype">
				<dc:subject>
					<xsl:value-of select="//ms:toolServiceSubtype"/>
				</dc:subject>
			</xsl:if>
			<xsl:for-each select="//ms:textGenre">
				<dc:subject>
					<xsl:value-of select="(.)"/>
				</dc:subject>
			</xsl:for-each>
			<xsl:for-each select="//ms:textType">
				<dc:subject>
					<xsl:value-of select="(.)"/>
				</dc:subject>
			</xsl:for-each>
			<xsl:for-each select="//ms:audioGenre">
				<dc:subject>
					<xsl:value-of select="(.)"/>
				</dc:subject>
			</xsl:for-each>
			<xsl:for-each select="//ms:videoGenre">
				<dc:subject>
					<xsl:value-of select="(.)"/>
				</dc:subject>
			</xsl:for-each>
			<xsl:for-each select="//ms:imageGenre">
				<dc:subject>
					<xsl:value-of select="(.)"/>
				</dc:subject>
			</xsl:for-each>
			<xsl:for-each select="//ms:annotationInfo">
				<dc:subject>
					<xsl:text>annotated corpus</xsl:text>
				</dc:subject>
			</xsl:for-each>
			<!-- 	END OF dc:subject 		-->
			<!-- 	dc:description 		-->
			<xsl:for-each select="./ms:identificationInfo/ms:description">
				<dc:description>
					<xsl:attribute name="xml:lang"><xsl:value-of select="@lang"/></xsl:attribute>
					<xsl:value-of select="(.)"/>
					<xsl:if test="../ms:url">
						<xsl:text>. See more at </xsl:text>
						<xsl:value-of select="../ms:url"/>
						<xsl:text>.</xsl:text>
					</xsl:if>
				</dc:description>
			</xsl:for-each>
			<!-- 	END OF dc:description 		-->
			<!--	dc:publisher		-->
			<dc:publisher>
				<xsl:text>CLARIN-EL (Greek CLARIN network) at Athena RC</xsl:text>
			</dc:publisher>
			<!--	dc:publisher		-->
			<!--	dc:contributor		-->
			<dc:contributor xsi:type="olac:role" olac:code="depositor">
				<xsl:choose>
					<xsl:when test="//ms:metadataInfo[ms:source[contains(.,'athena.clarin.gr')]]">
						<xsl:text>ATHENA RC</xsl:text>
					</xsl:when>
					<xsl:when test="//ms:metadataInfo[ms:source[contains(.,'auth.clarin.gr')]]">
						<xsl:text>Aristotle University of Thessaloniki</xsl:text>
					</xsl:when>
					<xsl:when test="//ms:metadataInfo[ms:source[contains(.,'uoa.clarin.gr')]]">
						<xsl:text>National and Kapodistrian University of Athens</xsl:text>
					</xsl:when>
					<xsl:when test="//ms:metadataInfo[ms:source[contains(.,'aegean.clarin.gr')]]">
						<xsl:text>University of the Aegean</xsl:text>
					</xsl:when>
					<xsl:when test="//ms:metadataInfo[ms:source[contains(.,'keg.clarin.gr')]]">
						<xsl:text>CENTRE FOR THE GREEK LANGUAGE</xsl:text>
					</xsl:when>
					<xsl:when test="//ms:metadataInfo[ms:source[contains(.,'demokritos.clarin.gr')]]">
						<xsl:text>NCSR ‘Demokritos’</xsl:text>
					</xsl:when>
					<xsl:when test="//ms:metadataInfo[ms:source[contains(.,'ionion.clarin.gr')]]">
						<xsl:text>Ionian University</xsl:text>
					</xsl:when>
					<xsl:when test="//ms:metadataInfo[ms:source[contains(.,'ekke.clarin.gr')]]">
						<xsl:text>NATIONAL CENTRE FOR SOCIAL RESEARCH</xsl:text>
					</xsl:when>
					<xsl:when test="//ms:metadataInfo[ms:source[contains(.,'panteion.clarin.gr')]]">
						<xsl:text>Panteion University of Social and Political Sciences</xsl:text>
					</xsl:when>
					<xsl:when test="//ms:metadataInfo[ms:source[contains(.,'clarin.clarin.gr')]]">
						<xsl:text>ATHENA RC</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="//ms:metadataInfo/ms:source"/>
					</xsl:otherwise>
				</xsl:choose>

			</dc:contributor>

			<xsl:for-each select="//ms:annotationInfo/ms:annotator">
				<dc:contributor xsi:type="olac:role" olac:code="annotator">
					<xsl:choose>
						<xsl:when test="ms:personInfo">
							<xsl:value-of select="ms:personInfo/ms:surname[@lang='en']"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="ms:personInfo/ms:givenName[@lang='en']"/>
							<xsl:if test="ms:personInfo/ms:affiliation/ms:organizationName[@lang='en']">
								<xsl:text>, </xsl:text>
								<xsl:value-of select="ms:personInfo/ms:affiliation/ms:organizationName[@lang='en']"/>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<xsl:if test="ms:organizationInfo/ms:departmentName[@lang='en']">
								<xsl:value-of select="ms:organizationInfo/ms:departmentName[@lang='en']"/>
								<xsl:text>, </xsl:text>
							</xsl:if>
							<xsl:value-of select="ms:organizationInfo/ms:organizationName[@lang='en']"/>
						</xsl:otherwise>
					</xsl:choose>
				</dc:contributor>
			</xsl:for-each>
			<!-- DECIDED commented out for now; a contact person is not a depositor and there's no point in keeping it without an email -->
			<!--			<xsl:for-each select="//ms:contactPerson">
				<dc:contributor xsi:type="olac:role" olac:code="depositor">
					<xsl:value-of select="ms:givenName[@lang='en']"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="ms:surname[@lang='en']"/>
					<xsl:if test="ms:affiliation/ms:organizationName[@lang='en']">
						<xsl:text>, </xsl:text>
						<xsl:value-of select="ms:affiliation/ms:organizationName[@lang='en']"/>
					</xsl:if>
				</dc:contributor>
			</xsl:for-each>
			-->
			<!--	END OF dc:contributor		-->
			<!--	dc:type 	-->
			<xsl:apply-templates select="ms:resourceComponentType//ms:resourceType"/>
			<xsl:apply-templates select="//ms:mediaType"/>
			<!--	END OF dc:type		-->
			<!--	dc:format		-->
			<!-- BEFORE MAPPING THIS WE MUST CORRECT SOME VALUES; and then we could add xsi:type with value 'IMT' -->
			<xsl:for-each select="//ms:resourceComponentType//ms:mimeType">
				<xsl:if test="not(parent::ms:inputInfo) and not(parent::ms:outputInfo)">
					<dc:format>
						<xsl:value-of select="."/>
					</dc:format>
				</xsl:if>
			</xsl:for-each>
			<!--	END OF dc:format		-->
			<!--	dc:identifier		-->
			<xsl:if test="./ms:identificationInfo/ms:PID">
				<dc:identifier xsi:type="dcterms:URI">
					<xsl:value-of select="./ms:identificationInfo/ms:PID"/>
				</dc:identifier>
			</xsl:if>
			<!--	END OF dc:identifier		-->
			<!--	dc:source		-->
			<xsl:for-each select="//ms:originalSource/ms:targetResourceNameURI">
				<dc:source>
					<xsl:value-of select="(.)"/>
				</dc:source>
			</xsl:for-each>
			<!--	END OF dc:source		-->
			<!--	dc:language		-->
			<xsl:for-each select="//ms:languageInfo">
				<dc:language>
					<!-- TODO: map the two-letter code to the three-letter one -->
					<!--<dc:language xsi:type="olac:language">
					<xsl:attribute name="olac:code">
					<xsl:apply-templates select="./ms:languageId"/>
					</xsl:attribute>-->
					<xsl:value-of select="./ms:languageName"/>
				</dc:language>
			</xsl:for-each>
			<xsl:for-each select="//ms:languageInfo/ms:languageVarietyInfo">
				<dc:language>
					<xsl:value-of select="./ms:languageVarietyName"/>
				</dc:language>
			</xsl:for-each>
			<xsl:for-each select="//ms:inputInfo/ms:languageName">
				<dc:language>
					<!-- TODO: map the two-letter code to the three-letter one -->
					<!--<dc:language xsi:type="olac:language">
					<xsl:attribute name="olac:code">
					<xsl:apply-templates select="./ms:languageId"/>
					</xsl:attribute>-->
					<xsl:value-of select="."/>
				</dc:language>
			</xsl:for-each>
			<xsl:for-each select="//ms:inputInfo/ms:languageVarietyName">
				<dc:language>
					<xsl:value-of select="."/>
				</dc:language>
			</xsl:for-each>
			<!--	END OF dc:language		-->
			<!--	START OF dc:relation	-->
			<xsl:for-each select="//ms:relationInfo[not(contains(
						concat(' ', $specialRelationTypes, ' '),
						concat(' ', ms:relationType, ' ')
				))]">
				<dc:relation>
					<xsl:value-of select="ms:relatedResource/ms:targetResourceNameURI"/>
				</dc:relation>
			</xsl:for-each>
			<!--			END OF dc:relation		-->
			<!--	dcterms:alternative		-->
			<xsl:for-each select="./ms:identificationInfo/ms:resourceShortName">
				<dcterms:alternative>
					<xsl:attribute name="xml:lang"><xsl:value-of select="@lang"/></xsl:attribute>
					<xsl:value-of select="(.)"/>
				</dcterms:alternative>
			</xsl:for-each>
			<!--	END OF dcterms:alternative		-->
			<!--	dcterms:created		-->
			<xsl:if test="ms:resourceCreationInfo">
				<xsl:choose>
					<xsl:when test="ms:resourceCreationInfo/ms:creationStartDate and ms:resourceCreationInfo/ms:creationEndDate">
						<dcterms:created>
							<xsl:value-of select="concat(ms:resourceCreationInfo/ms:creationStartDate, ' / ', ms:resourceCreationInfo/ms:creationEndDate)"/>
						</dcterms:created>
					</xsl:when>
					<xsl:when test="ms:resourceCreationInfo/ms:creationStartDate">
						<dcterms:created xsi:type="dcterms:W3CDTF">
							<xsl:value-of select="ms:resourceCreationInfo/ms:creationStartDate"/>
						</dcterms:created>
					</xsl:when>
					<xsl:when test="ms:resourceCreationInfo/ms:creationEndDate">
						<dcterms:created xsi:type="dcterms:W3CDTF">
							<xsl:value-of select="ms:resourceCreationInfo/ms:creationEndDate"/>
						</dcterms:created>
					</xsl:when>
				</xsl:choose>
			</xsl:if>
			<!--	END OF dcterms:created		-->
			<!--	END OF dcterms:available		-->
			<xsl:if test="ms:distributionInfo/ms:availabilityStartDate">
				<dcterms:available>
					<xsl:value-of select="ms:distributionInfo/ms:availabilityStartDate"/>
				</dcterms:available>
			</xsl:if>
			<!--	END OF dcterms:available		-->
			<!--	dcterms:extent		-->
			<xsl:for-each select="//ms:sizeInfo">
				<dcterms:extent>
					<xsl:value-of select="concat(ms:size, ' ', ms:sizeUnit)"/>
				</dcterms:extent>
			</xsl:for-each>
			<!--	END OF dcterms:extent		-->
			<!--	dcterms:medium		-->
			<xsl:for-each select="./ms:distributionInfo/ms:licenceInfo/ms:distributionAccessMedium">
				<dcterms:medium>
					<xsl:value-of select="(.)"/>
				</dcterms:medium>
			</xsl:for-each>
			<!--	END OF dcterms:medium		-->
			<!--	START OF dcterms:isVersionOf		-->
			<xsl:for-each select="//ms:relationInfo[contains(
						concat(' ', $isVersionOfTypes, ' '),
						concat(' ', ms:relationType, ' ')
				)]">
				<dcterms:isVersionOf>
					<xsl:value-of select="./ms:relatedResource/ms:targetResourceNameURI"/>
				</dcterms:isVersionOf>
			</xsl:for-each>
			<!--	END OF dcterms:isVersionOf		-->
			<!--	START OF dcterms:hasVersion		-->
			<xsl:for-each select="//ms:relationInfo[ms:relationType='hasAlignedVersion']">
				<dcterms:hasVersion>
					<xsl:value-of select="./ms:relatedResource/ms:targetResourceNameURI"/>
				</dcterms:hasVersion>
			</xsl:for-each>
			<!--	END OF dcterms:hasVersion		-->
			<!--	START OF dcterms:isPartOf		-->
			<xsl:for-each select="//ms:relationInfo[contains(
						concat(' ', $isPartOfTypes, ' '),
						concat(' ', ms:relationType, ' ')
				)]">
				<dcterms:isPartOf>
					<xsl:value-of select="./ms:relatedResource/ms:targetResourceNameURI"/>
				</dcterms:isPartOf>
			</xsl:for-each>
			<!--	END OF dcterms:isPartOf		-->
			<!--	START OF dcterms:hasPart		-->
			<xsl:for-each select="//ms:relationInfo[contains(
						concat(' ', $hasPartTypes, ' '),
						concat(' ', ms:relationType, ' ')
				)]">
				<dcterms:hasPart>
					<xsl:value-of select="./ms:relatedResource/ms:targetResourceNameURI"/>
				</dcterms:hasPart>
			</xsl:for-each>
			<!--	END OF dcterms:hasPart		-->
			<!--	dcterms:spatial		-->
			<xsl:for-each select="//ms:geographicCoverage">
				<dcterms:spatial>
					<xsl:value-of select="."/>
				</dcterms:spatial>
			</xsl:for-each>
			<!--	END OF dcterms:spatial		-->
			<!--	dcterms:temporal		-->
			<xsl:for-each select="//ms:timeCoverage">
				<dcterms:temporal>
					<xsl:value-of select="."/>
				</dcterms:temporal>
			</xsl:for-each>
			<!--	END OF dcterms:temporal		-->
			<!--	dcterms:rightsHolder		-->
			<xsl:for-each select="ms:distributionInfo/ms:iprHolder">
				<dcterms:rightsHolder>
					<xsl:text>IPR Holder: </xsl:text>
					<xsl:if test="./ms:personInfo">
						<xsl:value-of select="./ms:personInfo/ms:givenName[@lang='en']"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="./ms:personInfo/ms:surname[@lang='en']"/>
						<xsl:if test="./ms:personInfo/ms:affiliation/ms:organizationName[@lang='en']">
							<xsl:text>, </xsl:text>
							<xsl:value-of select="./ms:personInfo/ms:affiliation/ms:organizationName[@lang='en']"/>
						</xsl:if>
					</xsl:if>
					<xsl:if test="./ms:organizationInfo/ms:organizationName[@lang='en']">
						<xsl:value-of select="./ms:organizationInfo/ms:organizationName[@lang='en']"/>
					</xsl:if>
				</dcterms:rightsHolder>
			</xsl:for-each>
			<!--	END OF dcterms:rightsHolder		-->
			<!--	dcterms:accessRights		-->
			<xsl:for-each select="//ms:licence">
				<xsl:if test="*|node()='termsOfService' or *|node()='other' or *|node()='proprietary' or *|node()='underNegotiation' or *|node()='openForReuseWithRestrictions' ">
					<dcterms:accessRights>
						<xsl:value-of select="."/>
					</dcterms:accessRights>
				</xsl:if>
			</xsl:for-each>
			<xsl:for-each select="//ms:licence">
				<xsl:if test="../ms:restrictionsOfUse or ../ms:userNature or ../ms:membershipInfo or ../ms:fee">
					<dcterms:accessRights>
						<xsl:text>For licence "</xsl:text>
						<xsl:value-of select="."/>
						<xsl:text>"</xsl:text>
						<xsl:if test="../ms:restrictionsOfUse">
							<xsl:text>, restrictions of use: </xsl:text>
							<xsl:for-each select="../ms:restrictionsOfUse">
								<xsl:value-of select="."/>
								<xsl:if test="not(position()=last())">
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:for-each>
						</xsl:if>
						<xsl:if test="../ms:userNature">
							<xsl:text>, user nature: </xsl:text>
							<xsl:for-each select="../ms:userNature">
								<xsl:value-of select="."/>
								<xsl:if test="not(position()=last())">
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:for-each>
						</xsl:if>
						<xsl:for-each select="../ms:membershipInfo">
							<xsl:if test="ms:member and ms:membershipInstitution">
								<xsl:text>, for </xsl:text>
								<xsl:if test="ms:member='False' or ms:member='false'">
									<xsl:text>Non </xsl:text>
								</xsl:if>
								<xsl:text>Members of </xsl:text>
								<xsl:for-each select="ms:membershipInstitution">
									<xsl:value-of select="."/>
									<xsl:if test="not(position()=last())">
										<xsl:text>, </xsl:text>
									</xsl:if>
								</xsl:for-each>
							</xsl:if>
						</xsl:for-each>
						<xsl:if test="../ms:fee">
							<xsl:text>, fee: </xsl:text>
							<xsl:value-of select="../ms:fee"/>
						</xsl:if>
					</dcterms:accessRights>
				</xsl:if>
			</xsl:for-each>
			<!--	END OF dcterms:accessRights		-->
			<!--	dcterms:license		-->
			<xsl:for-each select="//ms:licence">
				<xsl:if test="*|node()!='termsOfService' and *|node()!='other' and *|node()!='proprietary' and *|node()!='underNegotiation' and *|node()!='openForReuseWithRestrictions' ">
					<dc:license>
						<xsl:value-of select="."/>
					</dc:license>
				</xsl:if>
			</xsl:for-each>
			<!--	END OF dcterms:license		-->
			<!--	dcterms:bibliographicCitation		-->
			<xsl:if test="ms:resourceDocumentationInfo/ms:documentation">
				<dcterms:bibliographicCitation>
					<xsl:choose>
						<xsl:when test="ms:resourceDocumentationInfo/ms:documentation/ms:documentInfo">
							<xsl:if test="ms:resourceDocumentationInfo/ms:documentation/ms:documentInfo/ms:author">
								<xsl:value-of select="ms:resourceDocumentationInfo/ms:documentation/ms:documentInfo/ms:author"/>
								<xsl:text>, </xsl:text>
							</xsl:if>
							<xsl:value-of select="ms:resourceDocumentationInfo/ms:documentation/ms:documentInfo/ms:title"/>
							<xsl:if test="ms:resourceDocumentationInfo/ms:documentation/ms:documentInfo/ms:url">
								<xsl:text>, </xsl:text>
								<xsl:value-of select="ms:resourceDocumentationInfo/ms:documentation/ms:documentInfo/ms:url"/>
							</xsl:if>
							<xsl:if test="ms:resourceDocumentationInfo/ms:documentation/ms:documentInfo/ms:journal">
								<xsl:text>. In: </xsl:text>
								<xsl:value-of select="ms:resourceDocumentationInfo/ms:documentation/ms:documentInfo/ms:journal"/>
							</xsl:if>
							<xsl:if test="ms:resourceDocumentationInfo/ms:documentation/ms:documentInfo/ms:volume">
								<xsl:value-of select="ms:resourceDocumentationInfo/ms:documentation/ms:documentInfo/ms:volume"/>
							</xsl:if>
							<xsl:if test="ms:resourceDocumentationInfo/ms:documentation/ms:documentInfo/ms:pages">
								<xsl:text>, pp.</xsl:text>
								<xsl:value-of select="ms:resourceDocumentationInfo/ms:documentation/ms:documentInfo/ms:pages"/>
							</xsl:if>
							<xsl:if test="ms:resourceDocumentationInfo/ms:documentation/ms:documentInfo/ms:conference">
								<xsl:text>, </xsl:text>
								<xsl:value-of select="ms:resourceDocumentationInfo/ms:documentation/ms:documentInfo/ms:conference"/>
							</xsl:if>
							<xsl:if test="ms:resourceDocumentationInfo/ms:documentation/ms:documentInfo/ms:year">
								<xsl:text>, </xsl:text>
								<xsl:value-of select="ms:resourceDocumentationInfo/ms:documentation/ms:documentInfo/ms:year"/>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="ms:resourceDocumentationInfo/ms:documentation/ms:documentUnstructured"/>
						</xsl:otherwise>
					</xsl:choose>
				</dcterms:bibliographicCitation>
			</xsl:if>
			<!--	END OF dcterms:bibliographicCitation		-->
		</olac:olac>
	</xsl:template>
</xsl:stylesheet>
