<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xslt="http://xml.apache.org/xsltm"
    xmlns:ms="http://w3id.org/meta-share/meta-share/"
    xmlns:elrc="http://www.elrc-share.eu/ELRC-SHARE_SCHEMA/v2.0/"
    xmlns:elg="http://european-language-grid.eu" xmlns:exsl="http://exslt.org/common"
    xmlns:str="http://exslt.org/strings" xmlns:func="http://exslt.org/functions"
    exclude-result-prefixes="xs elrc elg exsl xslt" version="1.0"
    extension-element-prefixes="func str">

    <xsl:output encoding="UTF-8" indent="yes" method="xml"/>
    <xsl:variable name="ontology">
        <xsl:text>http://w3id.org/meta-share/meta-share/</xsl:text>
    </xsl:variable>
    <xsl:variable name="resourceUpperType">
        <xsl:value-of
            select="translate(substring($resourceType, 1, 1), 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
        <xsl:value-of select="substring($resourceType, 2)"/>
    </xsl:variable>
    <xsl:variable name="ELG">
        <xsl:text>http://w3id.org/meta-share/meta-share/ELG-SHARE</xsl:text>
    </xsl:variable>
    <xsl:variable name="resourceType">
        <xsl:value-of select="/elrc:resourceInfo/elrc:resourceComponentType//elrc:resourceType"/>
    </xsl:variable>

    <xsl:template name="str:tokenize">
        <xsl:param name="string" select="''"/>
        <xsl:param name="delimiters" select="' &#x9;&#xA;'"/>
        <xsl:choose>
            <xsl:when test="not($string)"/>
            <xsl:when test="not($delimiters)">
                <xsl:call-template name="str:_tokenize-characters">
                    <xsl:with-param name="string" select="$string"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="str:_tokenize-delimiters">
                    <xsl:with-param name="string" select="$string"/>
                    <xsl:with-param name="delimiters" select="$delimiters"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="str:_tokenize-characters">
        <xsl:param name="string"/>
        <xsl:if test="$string">
            <token>
                <xsl:value-of select="substring($string, 1, 1)"/>
            </token>
            <xsl:call-template name="str:_tokenize-characters">
                <xsl:with-param name="string" select="substring($string, 2)"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <xsl:template name="str:_tokenize-delimiters">
        <xsl:param name="string"/>
        <xsl:param name="delimiters"/>
        <xsl:variable name="delimiter" select="substring($delimiters, 1, 1)"/>
        <xsl:choose>
            <xsl:when test="not($delimiter)">
                <xsl:element name="token">
                    <xsl:value-of select="$string"/>
                </xsl:element>
            </xsl:when>
            <xsl:when test="contains($string, $delimiter)">
                <xsl:if test="not(starts-with($string, $delimiter))">
                    <xsl:call-template name="str:_tokenize-delimiters">
                        <xsl:with-param name="string" select="substring-before($string, $delimiter)"/>
                        <xsl:with-param name="delimiters" select="substring($delimiters, 2)"/>
                    </xsl:call-template>
                </xsl:if>
                <xsl:call-template name="str:_tokenize-delimiters">
                    <xsl:with-param name="string" select="substring-after($string, $delimiter)"/>
                    <xsl:with-param name="delimiters" select="$delimiters"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="str:_tokenize-delimiters">
                    <xsl:with-param name="string" select="$string"/>
                    <xsl:with-param name="delimiters" select="substring($delimiters, 2)"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <func:function name="elg:upper-case" as="xs:string">
        <xsl:param name="value"/>
        <func:result>
            <xsl:value-of
                select="translate(substring($value, 1, 1), 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
            <xsl:value-of select="substring($value, 2)"/>
        </func:result>
    </func:function>
    <func:function name="elg:capitilize" as="xs:string">
        <xsl:param name="value"/>
        <func:result>
            <xsl:value-of
                select="concat(elg:upper-case(substring($value, 1, 1)), substring($value, 2))"/>
        </func:result>
    </func:function>
    <func:function name="elg:ontology" as="xs:string">
        <xsl:param name="element"/>
        <xsl:param name="key"/>
        <func:result>
            <xsl:value-of
                select="document('elrc2ELGMap.xml')/map/*[name() = $element]/item[@key = $key]/text()"
            />
        </func:result>
    </func:function>


    <!-- KEY/VALUE mappings -->

    <xsl:variable name="LRSubclass">
        <lrTypes>
            <lrType key="corpus" value="Corpus"/>
            <lrType key="toolService" value="ToolService"/>
            <lrType key="languageDescription" value="LanguageDescription"/>
            <lrType key="lexicalConceptualResource" value="LexicalConceptualResource"/>
        </lrTypes>
    </xsl:variable>
    <xsl:key name="subclassByRtype" match="*" use="string(@key)"/>

    <xsl:template match="/">
        <xsl:call-template name="MetadataRecord"/>
    </xsl:template>

    <xsl:template name="language">
        <xsl:variable name="languageId">
            <xsl:value-of select="elrc:languageId"/>
        </xsl:variable>
        <xsl:variable name="tokens" as="nodeset()">
            <xsl:call-template name="str:tokenize">
                <xsl:with-param name="string">
                    <xsl:value-of select="elrc:languageId"/>
                </xsl:with-param>
                <xsl:with-param name="delimiters">
                    <xsl:text>-</xsl:text>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:variable>
        <ms:language>
            <ms:languageTag>
                <xsl:value-of select="elrc:languageId"/>
            </ms:languageTag>
            <ms:languageId>
                <xsl:value-of select="exsl:node-set($tokens)/token[1]"/>
            </ms:languageId>
        </ms:language>
    </xsl:template>

    <xsl:template name="keyword">
        <xsl:if
            test="/elrc:resourceInfo/elrc:resourceComponentType/elrc:corpusInfo/elrc:corpusMediaType/elrc:corpusTextInfo/elrc:lingualityInfo/elrc:lingualityType = 'bilingual'">
            <ms:keyword xml:lang="en">parallel corpus</ms:keyword>
            <ms:keyword xml:lang="en">machine translation</ms:keyword>
        </xsl:if>
        <ms:keyword xml:lang="en">elrc data</ms:keyword>
    </xsl:template>

    <xsl:template name="domain">
        <xsl:for-each select="/elrc:resourceInfo/elrc:resourceComponentType//elrc:domainInfo">
            <ms:domain>
                <xsl:choose>
                    <xsl:when test="elrc:subdomain">
                        <ms:categoryLabel xml:lang="en">
                            <xsl:value-of select="elrc:subdomain"/>
                        </ms:categoryLabel>
                        <ms:DomainIdentifier
                            ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/EUROVOC">
                            <xsl:value-of select="elrc:subdomainId"/>
                        </ms:DomainIdentifier>
                    </xsl:when>
                    <xsl:otherwise>
                        <ms:categoryLabel xml:lang="en">
                            <xsl:value-of select="elrc:domain"/>
                        </ms:categoryLabel>
                        <ms:DomainIdentifier
                            ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/EUROVOC">
                            <xsl:value-of select="elrc:domainId"/>
                        </ms:DomainIdentifier>
                    </xsl:otherwise>
                </xsl:choose>
            </ms:domain>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="fundingProject">
        <xsl:for-each select="/elrc:resourceInfo/elrc:resourceCreationInfo/elrc:fundingProject">
            <ms:fundingProject>
                <ms:projectName xml:lang="en">
                    <xsl:value-of select="elrc:projectName"/>
                </ms:projectName>
                <xsl:if test="elrc:url">
                    <ms:website>
                        <xsl:value-of select="elrc:url"/>
                    </ms:website>
                </xsl:if>
                <xsl:if test="elrc:projectID">
                    <ms:grantNumber>
                        <xsl:value-of select="elrc:projectID/text()"/>
                    </ms:grantNumber>
                </xsl:if>
                <xsl:if test="elrc:fundingType">
                    <ms:fundingType>
                        <xsl:value-of select="elg:ontology('fundingType', elrc:fundingType)"/>
                    </ms:fundingType>
                </xsl:if>
                <xsl:for-each select="elrc:funder">
                    <ms:funder>
                        <ms:Organization>
                            <ms:actorType>Organization</ms:actorType>
                            <ms:organizationName xml:lang="en">
                                <xsl:value-of select="."/>
                            </ms:organizationName>
                        </ms:Organization>
                    </ms:funder>
                </xsl:for-each>
            </ms:fundingProject>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="creation">
        <xsl:if
            test="elrc:resourceInfo/elrc:resourceComponentType//elrc:creationInfo[1]/elrc:creationMode">
            <ms:creationMode>
                <xsl:value-of
                    select="elg:ontology('creationMode', elrc:resourceInfo/elrc:resourceComponentType//elrc:creationInfo[1]/elrc:creationMode)"
                />
            </ms:creationMode>
            <xsl:for-each
                select="/elrc:resourceInfo/elrc:resourceComponentType//elrc:creationInfo[1]/elrc:creationModeDetails">
                <ms:creationDetails xml:lang="en">
                    <xsl:value-of select="."/>
                </ms:creationDetails>
            </xsl:for-each>
            <xsl:for-each
                select="/elrc:resourceInfo/elrc:resourceComponentType//elrc:creationInfo[1]/elrc:originalSource">
                <ms:hasOriginalSource>
                    <ms:resourceName xml:lang="en">
                        <xsl:value-of select="elrc:targetResourceNameURI"/>
                    </ms:resourceName>
                </ms:hasOriginalSource>
            </xsl:for-each>
            <xsl:for-each
                select="/elrc:resourceInfo/elrc:resourceComponentType//elrc:creationInfo[1]/elrc:creationTool">
                <ms:isCreatedBy>
                    <ms:resourceName xml:lang="en">
                        <xsl:value-of select="elrc:targetResourceNameURI"/>
                    </ms:resourceName>
                </ms:isCreatedBy>
            </xsl:for-each>
            <xsl:if
                test="/elrc:resourceInfo/elrc:resourceComponentType/elrc:corpusInfo/elrc:corpusMediaType/elrc:corpusTextInfo/elrc:lingualityInfo/elrc:lingualityType = 'bilingual'">
                <ms:intendedApplication>
                    <ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/MachineTranslation
                    </ms:LTClassRecommended>
                </ms:intendedApplication>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <xsl:template name="documentation">
        <xsl:choose>
            <xsl:when test="elrc:documentUnstructured != ''">
                <ms:title xml:lang="en">
                    <xsl:value-of select="elrc:documentUnstructured"/>
                </ms:title>
            </xsl:when>
            <xsl:otherwise>
                <xsl:for-each select="elrc:documentInfo/elrc:title">
                    <ms:title>
                        <xsl:call-template name="attXMLLang">
                            <xsl:with-param name="lang">
                                <xsl:value-of select="./@lang"/>
                            </xsl:with-param>
                        </xsl:call-template>
                        <xsl:value-of select="."/>
                    </ms:title>
                </xsl:for-each>
                <xsl:if test="elrc:documentInfo/elrc:doi">
                    <ms:DocumentIdentifier
                        ms:DocumentIdentifierScheme="http://purl.org/spar/datacite/doi">
                        <xsl:value-of select="elrc:documentInfo/elrc:doi"/>
                    </ms:DocumentIdentifier>
                </xsl:if>
                <xsl:if test="elrc:documentInfo/elrc:url">
                    <ms:DocumentIdentifier
                        ms:DocumentIdentifierScheme="http://purl.org/spar/datacite/url">
                        <xsl:value-of select="elrc:documentInfo/elrc:url"/>
                    </ms:DocumentIdentifier>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="validation">
        <xsl:if test="/elrc:resourceInfo/elrc:validationInfo[1]">
            <ms:validated>
                <xsl:value-of select="/elrc:resourceInfo/elrc:validationInfo[1]/elrc:validated"/>
            </ms:validated>
        </xsl:if>
        <xsl:for-each select="/elrc:resourceInfo/elrc:validationInfo">
            <ms:validation>
                <xsl:for-each select="elrc:validationReport">
                    <ms:validationReport>
                        <xsl:call-template name="documentation"/>
                    </ms:validationReport>
                </xsl:for-each>
                <xsl:for-each select="elrc:validator">
                    <ms:validator>
                        <xsl:if test="elrc:personInfo">
                            <xsl:call-template name="person">
                                <xsl:with-param name="element">personInfo</xsl:with-param>
                            </xsl:call-template>
                        </xsl:if>
                        <xsl:if test="elrc:organizationInfo">
                            <xsl:call-template name="organization">
                                <xsl:with-param name="element">organizationInfo</xsl:with-param>
                            </xsl:call-template>
                        </xsl:if>
                    </ms:validator>
                </xsl:for-each>
            </ms:validation>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="textType">
        <xsl:variable name="value">
            <xsl:value-of select="."/>
        </xsl:variable>
        <xsl:if test="$value != 'other'">
            <ms:textType>
                <ms:categoryLabel xml:lang="en">
                    <xsl:value-of select="elg:ontology('textType', $value)"/>
                </ms:categoryLabel>
                <ms:TextTypeIdentifier>
                    <xsl:attribute name="ms:TextTypeClassificationScheme">
                        <xsl:value-of select="document('elrc2ELGMap.xml')/map/textType/@scheme"/>
                    </xsl:attribute>
                    <xsl:value-of
                        select="document('elrc2ELGMap.xml')/map/textType/item[@key = $value]/@id"/>
                </ms:TextTypeIdentifier>
            </ms:textType>
        </xsl:if>
    </xsl:template>

    <xsl:template name="TextGenre">
        <xsl:variable name="value">
            <xsl:value-of select="."/>
        </xsl:variable>
        <xsl:if test="$value != 'other'">
            <ms:TextGenre>
                <ms:categoryLabel xml:lang="en">
                    <xsl:value-of select="elg:ontology('textGenre', $value)"/>
                </ms:categoryLabel>
                <ms:TextGenreIdentifier>
                    <xsl:attribute name="ms:TextGenreClassificationScheme">
                        <xsl:value-of select="document('elrc2ELGMap.xml')/map/textGenre/@scheme"/>
                    </xsl:attribute>
                    <xsl:value-of
                        select="document('elrc2ELGMap.xml')/map/textGenre/item[@key = $value]/@id"/>
                </ms:TextGenreIdentifier>
            </ms:TextGenre>
        </xsl:if>
    </xsl:template>

    <xsl:template name="annotation">
        <ms:annotation>
            <ms:annotationType>
                <xsl:choose>
                    <xsl:when test="elg:ontology('annotationType', elrc:annotationType) != ''">
                        <ms:annotationTypeRecommended>
                            <xsl:value-of
                                select="elg:ontology('annotationType', elrc:annotationType)"/>
                        </ms:annotationTypeRecommended>
                    </xsl:when>
                    <xsl:otherwise>
                        <ms:annotationTypeOther>
                            <xsl:value-of select="elrc:annotationType"/>
                        </ms:annotationTypeOther>
                    </xsl:otherwise>
                </xsl:choose>
            </ms:annotationType>
            <xsl:if test="elrc:segmentationLevel">
                <ms:segmentationLevel>
                    <xsl:value-of select="elg:ontology('segmentationLevel', elrc:segmentationLevel)"
                    />
                </ms:segmentationLevel>
            </xsl:if>
            <xsl:if test="elrc:annotationStandoff">
                <ms:annotationStandoff>
                    <xsl:value-of select="elrc:annotationStandoff"/>
                </ms:annotationStandoff>
            </xsl:if>
            <xsl:if test="elrc:typesystem">
                <ms:tagset>
                    <ms:resourceName xml:lang="en">
                        <xsl:value-of select="elrc:typesystem"/>
                    </ms:resourceName>
                </ms:tagset>
            </xsl:if>
            <xsl:if test="elrc:theoreticModel">
                <ms:theoreticModel xml:lang="en">
                    <xsl:value-of select="elrc:theoreticModel"/>
                </ms:theoreticModel>
            </xsl:if>
            <xsl:if test="elrc:annotationMode">
                <ms:annotationMode>
                    <xsl:value-of select="elg:ontology('annotationMode', elrc:annotationMode)"/>
                </ms:annotationMode>
            </xsl:if>
            <xsl:if test="elrc:annotationModeDetails">
                <ms:annotationModeDetails xml:lang="en">
                    <xsl:value-of select="elrc:annotationModeDetails"/>
                </ms:annotationModeDetails>
            </xsl:if>
            <xsl:for-each select="elrc:annotationTool">
                <ms:isAnnotatedBy>
                    <ms:resourceName xml:lang="en">
                        <xsl:value-of select="elrc:targetResourceNameURI"/>
                    </ms:resourceName>
                </ms:isAnnotatedBy>
            </xsl:for-each>
            <xsl:for-each select="elrc:annotationManual">
                <ms:annotationReport>
                    <xsl:call-template name="documentation"/>
                </ms:annotationReport>
            </xsl:for-each>
        </ms:annotation>
    </xsl:template>

    <xsl:template name="MetadataRecord">
        <!-- Default Body -->
        <ms:MetadataRecord xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ https://live.european-language-grid.eu/metadata-schema/ELG-SHARE.xsd">
            <!--<ms:MetadataRecordIdentifier
                ms:MetadataRecordIdentifierScheme="http://w3id.org/meta-share/meta-share/elg"
                >ELG
                auto generated</ms:MetadataRecordIdentifier>-->
            <xsl:call-template name="metadataInfo">
                <xsl:with-param name="element" select="/elrc:resourceInfo/elrc:metadataInfo"/>
            </xsl:call-template>
            <ms:compliesWith>
                <xsl:value-of select="$ELG"/>
            </ms:compliesWith>
            <ms:sourceOfMetadataRecord>
                <ms:repositoryName xml:lang="en">ELRC-SHARE</ms:repositoryName>
                <ms:repositoryURL>https://elrc-share.eu</ms:repositoryURL>
            </ms:sourceOfMetadataRecord>
            <ms:sourceMetadataRecord>
                <ms:MetadataRecordIdentifier
                    ms:MetadataRecordIdentifierScheme="http://w3id.org/meta-share/meta-share/other">
                    <xsl:value-of select="/elrc:resourceInfo/elrc:identificationInfo/elrc:url"/>
                </ms:MetadataRecordIdentifier>
            </ms:sourceMetadataRecord>
            <!-- DescribedEntity -->
            <ms:DescribedEntity>
                <xsl:call-template name="describedEntity"/>
            </ms:DescribedEntity>
        </ms:MetadataRecord>
    </xsl:template>

    <xsl:template name="metadataInfo">
        <xsl:param name="element"/>
        <ms:metadataCreationDate>
            <xsl:value-of select="$element/elrc:metadataCreationDate"/>
        </ms:metadataCreationDate>
        <xsl:for-each select="$element/elrc:metadataLastDateUpdated">
            <ms:metadataLastDateUpdated>
                <xsl:value-of select="$element/elrc:metadataLastDateUpdated"/>
            </ms:metadataLastDateUpdated>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="attXMLLang">
        <xsl:param name="lang"/>
        <xsl:attribute name="xml:lang">
            <xsl:choose>
                <xsl:when test="$lang = ''">
                    <xsl:text>en</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$lang"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
    </xsl:template>

    <xsl:template name="person">
        <xsl:param name="element"/>
        <ms:Person>
            <ms:actorType>Person</ms:actorType>
            <ms:surname>
                <xsl:call-template name="attXMLLang">
                    <xsl:with-param name="lang">
                        <xsl:value-of select="*[name() = $element]/elrc:surname/@lang"/>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:value-of select="*[name() = $element]/elrc:surname"/>
            </ms:surname>
            <ms:givenName>
                <xsl:call-template name="attXMLLang">
                    <xsl:with-param name="lang">
                        <xsl:value-of select="*[name() = $element]/elrc:givenName/@lang"/>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:choose>
                    <xsl:when test="*[name() = $element]/elrc:givenName">
                        <xsl:value-of select="*[name() = $element]/elrc:givenName"/>
                    </xsl:when>
                    <xsl:otherwise>unspecified</xsl:otherwise>
                </xsl:choose>
            </ms:givenName>
            <xsl:if test="*[name() = $element]/elrc:communicationInfo/elrc:email">
                <ms:email>
                    <xsl:value-of select="*[name() = $element]/elrc:communicationInfo/elrc:email"/>
                </ms:email>
            </xsl:if>
        </ms:Person>
    </xsl:template>

    <xsl:template name="organization">
        <xsl:param name="element"/>
        <ms:Organization>
            <ms:actorType>Organization</ms:actorType>
            <ms:organizationName>
                <xsl:call-template name="attXMLLang">
                    <xsl:with-param name="lang">
                        <xsl:value-of select="*[name() = $element]/elrc:organizationName/@lang"/>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:value-of select="*[name() = $element]/elrc:organizationName"/>
            </ms:organizationName>
            <xsl:if test="*[name() = $element]/elrc:communicationInfo/elrc:url">
                <ms:website>
                    <xsl:value-of select="*[name() = $element]/elrc:communicationInfo/elrc:url"/>
                </ms:website>
            </xsl:if>
        </ms:Organization>
    </xsl:template>

    <xsl:template name="describedEntity">
        <ms:LanguageResource>
            <ms:entityType>LanguageResource</ms:entityType>
            <xsl:call-template name="identification"/>
            <ms:LRSubclass>
                <xsl:call-template name="LRSubclass"/>
            </ms:LRSubclass>
        </ms:LanguageResource>
    </xsl:template>

    <xsl:template name="identification">
        <xsl:for-each select="/elrc:resourceInfo/elrc:identificationInfo/elrc:resourceName">
            <ms:resourceName>
                <xsl:attribute name="xml:lang">
                    <xsl:value-of select="@lang"/>
                </xsl:attribute>
                <xsl:value-of select="."/>
            </ms:resourceName>
        </xsl:for-each>
        <xsl:for-each select="/elrc:resourceInfo/elrc:identificationInfo/elrc:resourceShortName">
            <ms:resourceShortName>
                <xsl:attribute name="xml:lang">
                    <xsl:value-of select="@lang"/>
                </xsl:attribute>
                <xsl:value-of select="."/>
            </ms:resourceShortName>
        </xsl:for-each>
        <xsl:for-each select="/elrc:resourceInfo/elrc:identificationInfo/elrc:description">
            <ms:description>
                <xsl:attribute name="xml:lang">
                    <xsl:value-of select="@lang"/>
                </xsl:attribute>
                <xsl:value-of select="."/>
            </ms:description>
        </xsl:for-each>
        <!-- HOW MANY -->
        <xsl:call-template name="version"/>
        <ms:additionalInfo>
            <ms:landingPage>
                <xsl:value-of select="/elrc:resourceInfo/elrc:identificationInfo/elrc:url"/>
            </ms:landingPage>
        </ms:additionalInfo>
        <xsl:call-template name="keyword"/>
        <xsl:call-template name="domain"/>
        <xsl:call-template name="fundingProject"/>
        <xsl:call-template name="creation"/>
        <xsl:call-template name="validation"/>
        <xsl:for-each select="/elrc:resourceInfo/elrc:resourceDocumentationInfo/elrc:documentation">
            <ms:isDocumentedBy>
                <xsl:call-template name="documentation"/>
            </ms:isDocumentedBy>
        </xsl:for-each>
    </xsl:template>
    <xsl:template name="version">
        <ms:version>
            <xsl:choose>
                <xsl:when test="elrc:resourceInfo/elrc:versionInfo/elrc:version != ''">
                    <xsl:value-of select="elrc:resourceInfo/elrc:versionInfo/elrc:version"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>unspecified</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </ms:version>
        <xsl:choose>
            <xsl:when test="elrc:resourceInfo/elrc:versionInfo/elrc:lastDateUpdated">
                <ms:versionDate>
                    <xsl:value-of select="elrc:resourceInfo/elrc:versionInfo/elrc:lastDateUpdated"/>
                </ms:versionDate>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="LRSubclass">
        <xsl:choose>
            <xsl:when test="$resourceType = 'corpus'">
                <xsl:call-template name="Corpus"/>
            </xsl:when>
            <xsl:when test="$resourceType = 'lexicalConceptualResource'">
                <xsl:call-template name="LexicalConceptualResource"/>
            </xsl:when>
            <xsl:when test="$resourceType = 'languageDescription'">
                <xsl:call-template name="LanguageDescription"/>
            </xsl:when>
            <xsl:when test="$resourceType = 'toolService'">
                <xsl:call-template name="ToolService"/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="ToolService">
        <ms:ToolService>
            <ms:lrType>ToolService</ms:lrType>
        </ms:ToolService>
    </xsl:template>
    <xsl:template name="LanguageDescription">
        <xsl:variable name="ldType">
            <xsl:value-of
                select="/elrc:resourceInfo/elrc:resourceComponentType//elrc:languageDescriptionType"
            />
        </xsl:variable>
        <ms:LanguageDescription>
            <ms:lrType>LanguageDescription</ms:lrType>
            <ms:ldSubclass>
                <xsl:value-of select="elg:ontology('language-desc', $ldType)"/>
            </ms:ldSubclass>
            <xsl:if test="$ldType = 'languageModel' or $ldType = 'grammar'">
                <ms:LanguageDescriptionSubclass>
                    <xsl:choose>
                        <xsl:when test="$ldType = 'languageModel'">
                            <ms:Model>
                                <ms:ldSubclassType>Model</ms:ldSubclassType>
                                <ms:modelFunction>
                                    <ms:modelFunctionRecommended>http://w3id.org/meta-share/meta-share/unspecified</ms:modelFunctionRecommended>
                                </ms:modelFunction>
                            </ms:Model>
                        </xsl:when>
                        <xsl:when test="$ldType = 'other'"> </xsl:when>
                    </xsl:choose>
                </ms:LanguageDescriptionSubclass>
            </xsl:if>
            <xsl:choose>
                <xsl:when test="$ldType = 'languageModel'">
                    <ms:unspecifiedPart>
                        <xsl:call-template name="UnspecifiedPart">
                            <xsl:with-param name="context"
                                select="/elrc:resourceInfo/elrc:resourceComponentType//elrc:languageDescriptionMediaType"
                            />
                        </xsl:call-template>
                    </ms:unspecifiedPart>
                    <xsl:for-each select="/elrc:resourceInfo/elrc:distributionInfo">
                        <xsl:call-template name="DatasetDistribution">
                            <xsl:with-param name="ld-type">model</xsl:with-param>
                        </xsl:call-template>
                    </xsl:for-each>
                </xsl:when>
            </xsl:choose>
            <xsl:for-each
                select="/elrc:resourceInfo/elrc:resourceComponentType/elrc:languageDescriptionInfo/elrc:languageDescriptionMediaType/*">
                <xsl:call-template name="MediaPart">
                    <xsl:with-param name="resourceType">LanguageDescription</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="/elrc:resourceInfo/elrc:distributionInfo">
                <xsl:call-template name="DatasetDistribution"/>
            </xsl:for-each>
            <xsl:call-template name="personalSensitive"/>
        </ms:LanguageDescription>
    </xsl:template>
    <xsl:template name="corpusSubclass">
        <ms:corpusSubclass>
            <xsl:choose>
                <xsl:when
                    test="/elrc:resourceInfo/elrc:resourceComponentType/elrc:corpusInfo/elrc:corpusMediaType/elrc:corpusTextInfo/elrc:annotationInfo">
                    <xsl:text>http://w3id.org/meta-share/meta-share/annotatedCorpus</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>http://w3id.org/meta-share/meta-share/rawCorpus</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </ms:corpusSubclass>
    </xsl:template>

    <xsl:template name="personalSensitive">
        <ms:personalDataIncluded>
            <xsl:choose>
                <xsl:when
                    test="/elrc:resourceInfo/elrc:distributionInfo[1]/elrc:personalDataIncluded = 'true'">
                    <xsl:text>http://w3id.org/meta-share/meta-share/yesP</xsl:text>
                </xsl:when>
                <xsl:when
                    test="/elrc:resourceInfo/elrc:distributionInfo[1]/elrc:personalDataIncluded = 'false'">
                    <xsl:text>http://w3id.org/meta-share/meta-share/noP</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>http://w3id.org/meta-share/meta-share/unspecified</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </ms:personalDataIncluded>
        <xsl:if test="/elrc:resourceInfo/elrc:distributionInfo[1]/elrc:personalDataAdditionalInfo">
            <ms:personalDataDetails xml:lang="en">
                <xsl:value-of
                    select="/elrc:resourceInfo/elrc:distributionInfo[1]/elrc:personalDataAdditionalInfo"
                />
            </ms:personalDataDetails>
        </xsl:if>
        <ms:sensitiveDataIncluded>
            <xsl:choose>
                <xsl:when
                    test="/elrc:resourceInfo/elrc:distributionInfo[1]/elrc:sensitiveDataIncluded = 'true'">
                    <xsl:text>http://w3id.org/meta-share/meta-share/yesS</xsl:text>
                </xsl:when>
                <xsl:when
                    test="/elrc:resourceInfo/elrc:distributionInfo[1]/elrc:sensitiveDataIncluded = 'false'">
                    <xsl:text>http://w3id.org/meta-share/meta-share/noS</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>http://w3id.org/meta-share/meta-share/unspecified</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </ms:sensitiveDataIncluded>
        <xsl:if test="/elrc:resourceInfo/elrc:distributionInfo[1]/elrc:sensitiveDataAdditionalInfo">
            <ms:sensitiveDataDetails xml:lang="en">
                <xsl:value-of
                    select="/elrc:resourceInfo/elrc:distributionInfo[1]/elrc:sensitiveDataAdditionalInfo"
                />
            </ms:sensitiveDataDetails>
        </xsl:if>
        <xsl:if test="/elrc:resourceInfo/elrc:resourceCreationInfo">
            <ms:anonymized>
                <xsl:choose>
                    <xsl:when
                        test="/elrc:resourceInfo/elrc:distributionInfo[1]/elrc:anonymized = 'true'">
                        <xsl:text>http://w3id.org/meta-share/meta-share/yesA</xsl:text>
                    </xsl:when>
                    <xsl:when
                        test="/elrc:resourceInfo/elrc:distributionInfo[1]/elrc:anonymized = 'false'">
                        <xsl:text>http://w3id.org/meta-share/meta-share/noA</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>http://w3id.org/meta-share/meta-share/unspecified</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </ms:anonymized>
            <xsl:if test="/elrc:resourceInfo/elrc:resourceCreationInfo/elrc:anonymizationDetails">
                <ms:anonymizationDetails xml:lang="en">
                    <xsl:value-of
                        select="/elrc:resourceInfo/elrc:resourceCreationInfo/elrc:anonymizationDetails"
                    />
                </ms:anonymizationDetails>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <xsl:template name="lingualityLanguage">
        <ms:lingualityType>
            <xsl:value-of
                select="elg:ontology('lingualityType', elrc:lingualityInfo/elrc:lingualityType)"/>
        </ms:lingualityType>
        <ms:multilingualityType>
            <xsl:choose>
                <xsl:when test="elrc:lingualityInfo/elrc:multilingualityType != ''">
                    <xsl:value-of
                        select="elg:ontology('multilingualityType', elrc:lingualityInfo/elrc:multilingualityType)"
                    />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>http://w3id.org/meta-share/meta-share/unspecified</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </ms:multilingualityType>
        <xsl:for-each select="elrc:lingualityInfo/elrc:multilingualityTypeDetails">
            <ms:multilingualityTypeDetails xml:lang="en">
                <xsl:value-of select="."/>
            </ms:multilingualityTypeDetails>
        </xsl:for-each>
        <xsl:for-each select="elrc:languageInfo">
            <xsl:call-template name="language"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="encodingLevel">
        <xsl:choose>
            <xsl:when test="elrc:resourceInfo//elrc:lexicalConceptualResourceEncodingInfo">
                <xsl:for-each
                    select="/elrc:resourceInfo//elrc:lexicalConceptualResourceEncodingInfo/elrc:encodingLevel">
                    <ms:encodingLevel>
                        <xsl:value-of select="elg:ontology('encodingLevel', .)"/>
                    </ms:encodingLevel>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <ms:encodingLevel>
                    <xsl:text>http://w3id.org/meta-share/meta-share/unspecified</xsl:text>
                </ms:encodingLevel>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="LexicalConceptualResource">
        <ms:LexicalConceptualResource>
            <ms:lrType>LexicalConceptualResource</ms:lrType>
            <ms:lcrSubclass>
                <xsl:value-of
                    select="elg:ontology('lrcSubclass', /elrc:resourceInfo/elrc:resourceComponentType/elrc:lexicalConceptualResourceInfo/elrc:lexicalConceptualResourceType)"
                />
            </ms:lcrSubclass>
            <xsl:call-template name="encodingLevel"/>
            <xsl:call-template name="lcrCommonInfo"/>
            <xsl:for-each
                select="/elrc:resourceInfo/elrc:resourceComponentType/elrc:lexicalConceptualResourceInfo/elrc:lexicalConceptualResourceMediaType/*">
                <xsl:call-template name="MediaPart">
                    <xsl:with-param name="resourceType">LexicalConceptualResource</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="/elrc:resourceInfo/elrc:distributionInfo">
                <xsl:call-template name="DatasetDistribution"/>
            </xsl:for-each>
            <xsl:call-template name="personalSensitive"/>
        </ms:LexicalConceptualResource>

    </xsl:template>
    <xsl:template name="lcrCommonInfo">
        <xsl:for-each
            select="/elrc:resourceInfo/elrc:resourceComponentType//elrc:lexicalConceptualResourceEncodingInfo/elrc:linguisticInformation">
            <ms:ContentType>
                <xsl:value-of select="elg:ontology('contentType', .)"/>
            </ms:ContentType>
        </xsl:for-each>
        <xsl:for-each
            select="/elrc:resourceInfo/elrc:resourceComponentType//elrc:lexicalConceptualResourceEncodingInfo/elrc:conformanceToStandardsBestPractices">
            <ms:compliesWith>
                <xsl:value-of select="elg:ontology('compliesWith', .)"/>
            </ms:compliesWith>
        </xsl:for-each>
        <xsl:for-each
            select="/elrc:resourceInfo/elrc:resourceComponentType//elrc:lexicalConceptualResourceEncodingInfo/elrc:externalRef">
            <ms:externalReference>
                <ms:resourceName xml:lang="en">
                    <xsl:value-of select="."/>
                </ms:resourceName>
            </ms:externalReference>
        </xsl:for-each>
        <xsl:for-each
            select="/elrc:resourceInfo/elrc:resourceComponentType//elrc:lexicalConceptualResourceEncodingInfo/elrc:theoreticModel">
            <ms:theoreticModel xml:lang="en">
                <xsl:value-of select="."/>
            </ms:theoreticModel>
        </xsl:for-each>
        <xsl:for-each
            select="/elrc:resourceInfo/elrc:resourceComponentType//elrc:lexicalConceptualResourceEncodingInfo/elrc:extratextualInformation">
            <ms:extratextualInformation>
                <xsl:value-of select="elg:ontology('extratextualInformation', .)"/>
            </ms:extratextualInformation>
        </xsl:for-each>
        <xsl:for-each
            select="/elrc:resourceInfo/elrc:resourceComponentType//elrc:lexicalConceptualResourceEncodingInfo/elrc:extraTextualInformationUnit">
            <ms:extratextualInformationUnit>
                <xsl:value-of select="elg:ontology('extratextualInformationUnit', .)"/>
            </ms:extratextualInformationUnit>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="LexicalConceptualResourceTextPart">
        <ms:lcrMediaType>LexicalConceptualResourceTextPart</ms:lcrMediaType>
        <ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
        <xsl:call-template name="lingualityLanguage"/>
    </xsl:template>

    <xsl:template name="LanguageDescriptionTextPart">
        <ms:ldMediaType>LanguageDescriptionTextPart</ms:ldMediaType>
        <ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
        <xsl:call-template name="lingualityLanguage"/>
    </xsl:template>

    <xsl:template name="UnspecifiedPart">
        <xsl:param name="context"/>
        <xsl:for-each select="$context//elrc:multilingualityType">
            <ms:multilingualityType>
                <xsl:value-of select="elg:ontology('multilingualityType', .)"/>
            </ms:multilingualityType>
        </xsl:for-each>
        <xsl:for-each select="$context//elrc:multilingualityTypeDetails">
            <ms:multilingualityTypeDetails>
                <xsl:attribute name="xml:lang">
                    <xsl:value-of select="@lang"/>
                </xsl:attribute>
                <xsl:value-of select="."/>
            </ms:multilingualityTypeDetails>
        </xsl:for-each>
        <xsl:for-each select="$context//elrc:languageInfo">
            <xsl:call-template name="language"/>
        </xsl:for-each>
        <xsl:for-each select="$context//elrc:metalanguage">
            <xsl:call-template name="language"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="Corpus">
        <ms:Corpus>
            <ms:lrType>Corpus</ms:lrType>
            <xsl:call-template name="corpusSubclass"/>
            <xsl:for-each
                select="/elrc:resourceInfo/elrc:resourceComponentType/elrc:corpusInfo/elrc:corpusMediaType/*">
                <xsl:call-template name="MediaPart">
                    <xsl:with-param name="resourceType">Corpus</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
            <!-- start DatasetDistribution -->
            <xsl:apply-templates select="/elrc:resourceInfo/elrc:distributionInfo[1]"/>
            <!-- end DatasetDistribution -->
            <xsl:call-template name="personalSensitive"/>
        </ms:Corpus>
    </xsl:template>
    <xsl:template name="MediaPart">
        <xsl:param name="resourceType"/>
        <xsl:element name="{concat('ms:', elg:capitilize($resourceType), 'MediaPart')}">
            <xsl:variable name="textPartType">
                <xsl:value-of
                    select="concat($resourceType, elg:capitilize(./elrc:mediaType), 'Part')"/>
            </xsl:variable>
            <xsl:element name="ms:{$textPartType}">
                <xsl:choose>
                    <xsl:when test="$resourceType = 'Corpus'">
                        <xsl:call-template name="CorpusTextPart"/>
                    </xsl:when>
                    <xsl:when test="$resourceType = 'LexicalConceptualResource'">
                        <xsl:call-template name="LexicalConceptualResourceTextPart"/>
                    </xsl:when>
                    <xsl:when test="$resourceType = 'LanguageDescription'">
                        <xsl:call-template name="LanguageDescriptionTextPart"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:element>

        </xsl:element>
    </xsl:template>
    <xsl:template name="CorpusTextPart">
        <ms:corpusMediaType>
            <xsl:value-of select="concat('Corpus', elg:capitilize(./elrc:mediaType), 'Part')"/>
        </ms:corpusMediaType>
        <ms:mediaType>
            <xsl:value-of select="elg:ontology('mediaType', ./elrc:mediaType)"/>
        </ms:mediaType>
        <xsl:call-template name="lingualityLanguage"/>
        <!-- textType -->
        <xsl:for-each
            select="/elrc:resourceInfo/elrc:resourceComponentType/elrc:corpusInfo/elrc:corpusMediaType/elrc:corpusTextInfo/elrc:textClassificationInfo/elrc:textType">
            <xsl:call-template name="textType"/>
        </xsl:for-each>
        <!-- TextGenre -->
        <xsl:for-each
            select="/elrc:resourceInfo/elrc:resourceComponentType/elrc:corpusInfo/elrc:corpusMediaType/elrc:corpusTextInfo/elrc:textClassificationInfo/elrc:textGenre">
            <xsl:call-template name="TextGenre"/>
        </xsl:for-each>
        <!-- annotation -->
        <xsl:for-each
            select="/elrc:resourceInfo/elrc:resourceComponentType/elrc:corpusInfo/elrc:corpusMediaType/elrc:corpusTextInfo/elrc:annotationInfo">
            <xsl:call-template name="annotation"/>
        </xsl:for-each>
    </xsl:template>
    <xsl:template name="distributionUnspecifiedFeature">
        <ms:distributionUnspecifiedFeature>
            <xsl:choose>
                <xsl:when test="elrc:sizeInfo">
                    <xsl:for-each select="elrc:sizeInfo">
                        <ms:size>
                            <ms:amount>
                                <xsl:value-of select="elrc:size"/>
                            </ms:amount>
                            <ms:sizeUnit>
                                <xsl:choose>
                                    <xsl:when test="elg:ontology('sizeUnit', elrc:sizeUnit) != ''">
                                        <ms:sizeUnitRecommended>
                                            <xsl:value-of
                                                select="elg:ontology('sizeUnit', elrc:sizeUnit)"/>
                                        </ms:sizeUnitRecommended>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <ms:sizeUnitOther>
                                            <xsl:value-of select="elrc:sizeUnit"/>
                                        </ms:sizeUnitOther>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </ms:sizeUnit>
                        </ms:size>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                    <ms:size>
                        <ms:amount>0</ms:amount>
                        <ms:sizeUnit>
                            <ms:sizeUnitRecommended>http://w3id.org/meta-share/meta-share/unspecified</ms:sizeUnitRecommended>
                        </ms:sizeUnit>
                    </ms:size>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="elrc:textFormatInfo/elrc:dataFormat">
                    <xsl:for-each select="elrc:textFormatInfo/elrc:dataFormat">
                        <ms:dataFormat>
                            <xsl:choose>
                                <xsl:when test="elg:ontology('dataFormat', .) != ''">
                                    <ms:dataFormatRecommended>
                                        <xsl:value-of select="elg:ontology('dataFormat', .)"/>
                                    </ms:dataFormatRecommended>
                                </xsl:when>
                                <xsl:otherwise>
                                    <ms:dataFormatOther>
                                        <xsl:value-of select="."/>
                                    </ms:dataFormatOther>
                                </xsl:otherwise>
                            </xsl:choose>
                        </ms:dataFormat>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                    <ms:dataFormat>
                        <ms:dataFormatRecommended>http://w3id.org/meta-share/meta-share/unspecified</ms:dataFormatRecommended>
                    </ms:dataFormat>
                </xsl:otherwise>
            </xsl:choose>
        </ms:distributionUnspecifiedFeature>
    </xsl:template>
    <xsl:template name="distributionFeature">
        <ms:distributionTextFeature>
            <xsl:choose>
                <xsl:when test="elrc:sizeInfo">
                    <xsl:for-each select="elrc:sizeInfo">
                        <ms:size>
                            <ms:amount>
                                <xsl:value-of select="elrc:size"/>
                            </ms:amount>
                            <ms:sizeUnit>
                                <xsl:choose>
                                    <xsl:when test="elg:ontology('sizeUnit', elrc:sizeUnit) != ''">
                                        <ms:sizeUnitRecommended>
                                            <xsl:value-of
                                                select="elg:ontology('sizeUnit', elrc:sizeUnit)"/>
                                        </ms:sizeUnitRecommended>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <ms:sizeUnitOther>
                                            <xsl:value-of select="elrc:sizeUnit"/>
                                        </ms:sizeUnitOther>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </ms:sizeUnit>
                        </ms:size>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                    <ms:size>
                        <ms:amount>0</ms:amount>
                        <ms:sizeUnit>
                            <ms:sizeUnitRecommended>http://w3id.org/meta-share/meta-share/unspecified</ms:sizeUnitRecommended>
                        </ms:sizeUnit>
                    </ms:size>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="elrc:textFormatInfo/elrc:dataFormat">
                    <xsl:for-each select="elrc:textFormatInfo/elrc:dataFormat">
                        <ms:dataFormat>
                            <xsl:choose>
                                <xsl:when test="elg:ontology('dataFormat', .) != ''">
                                    <ms:dataFormatRecommended>
                                        <xsl:value-of select="elg:ontology('dataFormat', .)"/>
                                    </ms:dataFormatRecommended>
                                </xsl:when>
                                <xsl:otherwise>
                                    <ms:dataFormatOther>
                                        <xsl:value-of select="."/>
                                    </ms:dataFormatOther>
                                </xsl:otherwise>
                            </xsl:choose>
                        </ms:dataFormat>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                    <ms:dataFormat>
                        <ms:dataFormatRecommended>http://w3id.org/meta-share/meta-share/unspecified</ms:dataFormatRecommended>
                    </ms:dataFormat>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:for-each select="elrc:characterEncodingInfo/elrc:characterEncoding">
                <ms:characterEncoding>
                    <xsl:value-of select="elg:ontology('characterEncoding', .)"/>
                </ms:characterEncoding>
            </xsl:for-each>
        </ms:distributionTextFeature>
    </xsl:template>

    <xsl:template name="licenceTerms">
        <xsl:param name="licence"/>
        <xsl:param name="mapped-licence"/>
        <xsl:param name="mapped-url"/>
        <ms:licenceTerms>
            <ms:licenceTermsName xml:lang="en">
                <xsl:value-of select="$mapped-licence"/>
            </ms:licenceTermsName>
            <ms:licenceTermsURL>
                <xsl:value-of select="$mapped-url"/>
            </ms:licenceTermsURL>
            <!-- this will be resolved by ELG -->
            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/unspecified</ms:conditionOfUse>
        </ms:licenceTerms>
    </xsl:template>

    <xsl:template name="DatasetDistribution" match="//elrc:distributionInfo[1]">
        <xsl:param name="distribution">
            <!-- Corpus or LexicalConceptualResource -->
        </xsl:param>
        <xsl:param name="resourceType">
            <!-- Corpus or LexicalConceptualResource -->
        </xsl:param>
        <xsl:param name="ld-type"/>
        <ms:DatasetDistribution>
            <ms:DatasetDistributionForm>
                <xsl:choose>
                    <xsl:when test="elrc:distributionMedium">
                        <xsl:value-of
                            select="elg:ontology('distributionMedium', elrc:distributionMedium[1])"
                        />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>http://w3id.org/meta-share/meta-share/downloadable</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </ms:DatasetDistributionForm>
            <xsl:choose>
               <xsl:when test="elrc:downloadLocation">
                <xsl:choose>
                    <xsl:when
                        test="
                            elrc:licenceInfo[starts-with(elrc:licence, 'CC') or
                            elrc:licence = 'publicDomain' or elrc:licence = 'openUnder-PSI' or elrc:licence = 'NLOD-1.0' or elrc:licence = 'EUPL-1.1' or elrc:licence = 'LO-OL-v2']">
                        <ms:downloadLocation>
                            <xsl:value-of select="elrc:downloadLocation"/>
                        </ms:downloadLocation>
                    </xsl:when>
                    <xsl:otherwise>
                        <ms:accessLocation>
                            <xsl:value-of select="elrc:downloadLocation"/>
                        </ms:accessLocation>
                    </xsl:otherwise>
                </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <ms:accessLocation>
                        <xsl:value-of select="/elrc:resourceInfo/elrc:identificationInfo/elrc:url"/>
                    </ms:accessLocation>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:for-each select="//elrc:corpusTextInfo[1]">
                <xsl:call-template name="distributionFeature"/>
            </xsl:for-each>
            <xsl:for-each select="//elrc:lexicalConceptualResourceTextInfo[1]">
                <xsl:call-template name="distributionFeature"/>
            </xsl:for-each>
            <xsl:for-each select="//elrc:languageDescriptionTextInfo[1]">
                <xsl:choose>
                    <xsl:when test="$ld-type = 'model'">
                        <xsl:call-template name="distributionUnspecifiedFeature"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="distributionFeature"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
            <xsl:for-each select="elrc:licenceInfo">
                <xsl:variable name="licence">
                    <xsl:value-of select="elrc:licence"/>
                </xsl:variable>
                <xsl:variable name="ml">
                    <xsl:value-of
                        select="document('elrc2ELGMap.xml')/map/licence/item[@key = $licence]"/>
                </xsl:variable>
                <xsl:variable name="mu">
                    <xsl:value-of
                        select="document('elrc2ELGMap.xml')/map/licence/item[@key = $licence]/@url"
                    />
                </xsl:variable>
                <xsl:if test="$ml != ''">
                    <xsl:call-template name="licenceTerms">
                        <xsl:with-param name="licence" select="$licence"/>
                        <xsl:with-param name="mapped-licence" select="$ml"/>
                        <xsl:with-param name="mapped-url" select="$mu"/>
                    </xsl:call-template>
                </xsl:if>
            </xsl:for-each>
            <xsl:for-each select="elrc:attributionText">
                <ms:attributionText xml:lang="en">
                    <xsl:value-of select="."/>
                </ms:attributionText>
            </xsl:for-each>
            <xsl:for-each select="elrc:licenceInfo">
                <xsl:variable name="licence">
                    <xsl:value-of select="elrc:licence"/>
                </xsl:variable>
                <xsl:variable name="ml">
                    <xsl:value-of
                        select="document('elrc2ELGMap.xml')/map/licence/item[@key = $licence]"/>
                </xsl:variable>
                <xsl:variable name="mu">
                    <xsl:value-of
                        select="document('elrc2ELGMap.xml')/map/licence/item[@key = $licence]/@url"
                    />
                </xsl:variable>
                <xsl:if test="$ml = ''">
                    <xsl:call-template name="AccessRights"/>
                </xsl:if>
            </xsl:for-each>
        </ms:DatasetDistribution>
    </xsl:template>
    <xsl:template name="AccessRights">
        <ms:accessRights>
            <ms:categoryLabel xml:lang="en">
                <xsl:value-of select="elrc:licence"/>
            </ms:categoryLabel>
        </ms:accessRights>
    </xsl:template>
</xsl:stylesheet>
